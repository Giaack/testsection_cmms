const assetPageUrl: string = 'http://localhost:8091/asset'

const openOrCloseLateralMenu = () => {
    cy.url().should('equal', assetPageUrl)
    cy.get('[class="v-btn v-btn--fab v-btn--icon v-btn--round theme--dark v-size--default"]').click({force: true})
}

const startAddingNewAssetProcedure = () => {
    cy.url().should('equal', assetPageUrl)
    cy.get('[d="M19,13H13V19H11V13H5V11H11V5H13V11H19V13Z"]').click()
}

const insertNumeroSeriale = (numSeriale: string) => {
    cy.get('[class="v-label theme--light"]').contains('Numero seriale').siblings().type(numSeriale)
}

const insertDescrizione = (descrizione: string) => {
    cy.get('[class="v-label theme--light"]').contains('Descrizione').siblings().type(descrizione)
}

const selectCategoria = (categoria: string) => {
    cy.get('[d="M7,10L12,15L17,10H7Z"]').eq(1).click()
    cy.get('[class="v-list v-select-list v-sheet theme--light v-list--dense theme--light"]').eq(0).children().contains(categoria).click()
}

const selectStato = (stato: string) => {
    cy.get('[d="M7,10L12,15L17,10H7Z"]').eq(2).click()
    cy.get('[class="v-list v-select-list v-sheet theme--light v-list--dense theme--light"]').eq(1).children().contains(stato).click()
}

const selectEdificio = (edificio: string) => {
    cy.get('[class="v-stepper__content"]').eq(0).children().contains(edificio).click()
}

const selectPiano = (piano: string) => {
    cy.get('[class="v-stepper__content"]').eq(1).children().contains(piano).click()
}

const selectArea = (area: string) => {
    cy.get('[class="v-stepper__content"]').eq(2).children().contains(area).click()
}

const confirm = () => {
    cy.get('[class="v-btn v-btn--text theme--light v-size--default"]').contains('Conferma').click()
    cy.get('[class="v-card__actions"]').last().contains('Conferma').click()
}

const searchAsset = (options?: {toSearch?: string, tutti?: boolean, categoria?:string, strutture?: string}) => {

    cy.url().should('equal', assetPageUrl)
    cy.reload() // si ricarica la pagina per 'pulire' i field, ha senso per più ricerche in successione -> forse ha senso farlo nel test (?)
    cy.get('[class="text-body-2 text-sm-h6 font-weight-medium"]').click()

    if(options?.toSearch)
        cy.get('[class="v-text-field__slot"]').children('input').type(options?.toSearch)

    if(options?.tutti) {
        cy.get('[class="v-input--selection-controls__input"]').children('input').click({force: true})
    }
       
    if(options?.categoria) {
        cy.get('[d="M7,10L12,15L17,10H7Z"]').eq(0).click()
        cy.get('[class="v-list v-select-list v-sheet theme--light v-list--dense theme--light"]').children().contains(options?.categoria).click()
    }
    
    if(options?.strutture) {
        cy.get('[d="M7,10L12,15L17,10H7Z"]').eq(1).click()
        cy.get('[class="v-list v-select-list v-sheet theme--light v-list--dense theme--light"]').children().contains(options?.strutture).click()
    }
    cy.get('[class="text-body-2 text-sm-h6 font-weight-medium"]').click()

    // cy.get('[class="v-data-footer__pagination"]').should('not.have.text', '-') // controlliamo che il risultato della ricerca non sia 0
    // o forse sarebbe meglio mettere il check su '1-1 of 1', oppure altri modi?
}

const searchAssetAndExpectedTotal = (expectedNum: number, options?: {toSearch?: string, tutti?: boolean, categoria?:string, strutture?: string}) => {
    searchAsset(options)

    if(expectedNum > 10)
        cy.get('[class="v-data-footer__pagination"]').should('contain.text', '1-10 of ' + expectedNum.toString())
    else
    cy.get('[class="v-data-footer__pagination"]').should('contain.text', '1-' + expectedNum.toString() + ' of ' + expectedNum.toString())
}

const addNewAsset = (numSeriale: string, descrizione: string, categoria: string, stato: string, edificio: string, piano: string, area: string ) => {
    cy.url().should('equal', assetPageUrl)
    startAddingNewAssetProcedure()
    insertNumeroSeriale(numSeriale)
    insertDescrizione(descrizione)
    selectCategoria(categoria)
    selectStato(stato)
    selectEdificio(edificio)
    selectPiano(piano)
    selectArea(area)
    confirm()
}

// const modifyAsset = (modifiable?: {numSeriale?: string, descrizione?: string, categoria?: string, stato?: string, edificio?: string, piano?: string, area?: string},  ) => {}

const sostituisciAsset = (numSeriale: string, descrizione: string, categoria: string, stato: string, motivo?: string, options?:{toSearch?: string, tutti?: boolean, categoria?:string, strutture?: string}) => {
    searchAssetAndExpectedTotal(1, options)
    cy.get('[d="M9,3L5,7H8V14H10V7H13M16,17V10H14V17H11L15,21L19,17H16Z"]').click({force: true})
    if(motivo)
        cy.get('textarea').type(motivo)
    cy.get('[type="text"]').eq(4).type(numSeriale)
    cy.get('[type="text"]').eq(5).type(descrizione)
    cy.get('[d="M7,10L12,15L17,10H7Z"]').eq(3).click()
    cy.get('[class="v-list v-select-list v-sheet theme--light v-list--dense theme--light"]').last().children().contains(categoria).click()
    cy.get('[d="M7,10L12,15L17,10H7Z"]').eq(3).click()
    cy.get('[d="M7,10L12,15L17,10H7Z"]').eq(4).click()
    cy.get('[class="v-list v-select-list v-sheet theme--light v-list--dense theme--light"]').last().children().contains(stato).click()
    cy.get('[d="M7,10L12,15L17,10H7Z"]').eq(4).click()
    cy.get('[class="v-btn__content"]').contains('Modifica').click()
    cy.get('[class="v-btn__content"]').contains('Conferma').click()

}

export {
    openOrCloseLateralMenu,
    addNewAsset,
    searchAsset,
    searchAssetAndExpectedTotal,
    sostituisciAsset
}

