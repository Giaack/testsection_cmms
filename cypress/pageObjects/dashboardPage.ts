const loginPageUrl: string = 'http://localhost:8091/login'
const dashboardPageUrl: string = 'http://localhost:8091/dashboard'
const usersPageUrl: string = 'http://localhost:8091/users'
const odlPageUrl: string = 'http://localhost:8091/odl'
const plannedMaintenancePageUrl: string = 'http://localhost:8091/plannedMaintenance'
const maintainersPageUrl: string = 'http://localhost:8091/maintainers'
const structurePageUrl: string = 'http://localhost:8091/structure'
const assetPageUrl: string = 'http://localhost:8091/asset'
const customerPageUrl: string = 'http://localhost:8091/customer'
const supplierPageUrl: string = 'http://localhost:8091/supplier'
const rolesPageUrl: string = 'http://localhost:8091/roles'
const skillsPageUrl: string = 'http://localhost:8091/skills'
const assetHistoryPageUrl: string = 'http://localhost:8091/assetHistory'
const odlHistoryPageUrl: string = 'http://localhost:8091/ODLHistory'
const mpHistoryPageUrl: string = 'http://localhost:8091/MPHistory'

const openOrCloseLateralMenu = () => {
    cy.url().should('equal', dashboardPageUrl)
    cy.get('[class="v-btn v-btn--fab v-btn--icon v-btn--round theme--dark v-size--default"]').click({force: true})
}

const checkLoggedUser = (name: string, surname: string) => {
    openOrCloseLateralMenu()
    cy.get('[class="v-list-item__title"]').should('contain.text',name+" "+surname)
}

const changeLang = () => {
    cy.get('[class="elevation-0 v-btn v-btn--is-elevated v-btn--has-bg theme--dark v-size--small transparent"]').eq(0).click()
    cy.get('[class="v-list v-sheet theme--light v-list--dense"]').children().contains('en').click()
} 

const logOut = () => {
    cy.get('[class="elevation-0 v-btn v-btn--is-elevated v-btn--has-bg theme--dark v-size--small transparent"]').eq(1).click()
}

const checkLoggedUserRole = (role: string) => {
    openOrCloseLateralMenu()
    cy.get('[class="v-list-item__subtitle"]').should('contain.text', role)
}

const goToUsersPage = () => {
    openOrCloseLateralMenu()
    cy.get('[href="/users"]').click()
    cy.url().should('equal', usersPageUrl)
}

const goToOdlPage = () => {
    openOrCloseLateralMenu()
    cy.get('[href="/odl"]').click()
    cy.url().should('equal', odlPageUrl)
}

const goToPlannedMaintenancePage = () => {
    openOrCloseLateralMenu()
    cy.get('[href="/plannedMaintenance"]').click()
    cy.url().should('equal', plannedMaintenancePageUrl)
}

const goToManutentoriPage = () => {
    openRisorseSector()
    cy.get('[href="/maintainers"]').click()
    cy.url().should('equal', maintainersPageUrl)
}

const goToStructurePage = () => {
    openRisorseSector()
    cy.get('[href="/structure"]').click()
    cy.url().should('equal', structurePageUrl)
}

const goToAssetPage = () => {
    openRisorseSector()
    cy.get('[href="/asset"]').click()
    cy.url().should('equal', assetPageUrl)
}

const goToCustomerPage = () => {
    openRisorseSector()
    cy.get('[href="/customer"]').click()
    cy.url().should('equal', customerPageUrl)
}

const goToSupplierPage = () => {
    openRisorseSector()
    cy.get('[href="/supplier"]').click()
    cy.url().should('equal', supplierPageUrl)
}

const goToRolesPage = () => {
    openOrCloseLateralMenu()
    cy.get('[class="v-list-item__title"]').contains('Competenze').click()
    cy.get('[href="/roles"]').click()
    cy.url().should('equal', rolesPageUrl)
}

const goToSkillsPage = () => {
    openOrCloseLateralMenu()
    cy.get('[class="v-list-item__title"]').contains('Competenze').click()
    cy.get('[href="/skills"]').click()
    cy.url().should('equal', skillsPageUrl)
}

const goToAssetHistoryPage = () => {
    openOrCloseLateralMenu()
    cy.get('[class="v-list-item__title"]').contains('Storico').click()
    cy.get('[href="/assetHistory"]').click()
    cy.url().should('equal', assetHistoryPageUrl)
}

const goToODLHistoryPage = () => {
    openOrCloseLateralMenu()
    cy.get('[class="v-list-item__title"]').contains('Storico').click()
    cy.get('[href="/ODLHistory"]').click()
    cy.url().should('equal', odlHistoryPageUrl)
}

const goToMPHistoryPage = () => {
    openOrCloseLateralMenu()
    cy.get('[class="v-list-item__title"]').contains('Storico').click()
    cy.get('[href="/MPHistory"]').click()
    cy.url().should('equal', mpHistoryPageUrl)
}

const openRisorseSector = () => {
    openOrCloseLateralMenu()
    cy.get('[class="v-list-item__title"]').contains('Risorse').click()
}


export {
    openOrCloseLateralMenu,
    checkLoggedUser, 
    checkLoggedUserRole,
    goToUsersPage,
    goToOdlPage,
    goToPlannedMaintenancePage,
    goToManutentoriPage,
    goToStructurePage,
    goToAssetPage,
    goToCustomerPage,
    goToSupplierPage,
    goToRolesPage,
    goToSkillsPage,
    goToAssetHistoryPage,
    goToODLHistoryPage,
    goToMPHistoryPage,
    openRisorseSector, 
    logOut,
    changeLang
}