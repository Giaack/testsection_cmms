const clientiPageUrl: string = 'http://localhost:8091/customer'

const fillGeneralResearch = (generalString?: string) => {
    if(generalString)
        cy.get('label').contains('Ricerca generale').siblings('input').click().type(generalString)
}

const selectStructure = (structure?: string) => {
    if(structure) {
        cy.get('label').contains('Cerca per struttura').siblings('[type="text"]').click({multiple: true}).type(structure)
        cy.get('[class="v-list v-select-list v-sheet theme--light theme--light"]').children().contains(structure).click()
    }
}

const selectCliente = (cliente?: string) => {
    if(cliente) {
        cy.get('label').contains('Cerca per cliente').siblings('[type="text"]').click({multiple: true}).type(cliente)
        cy.get('[class="v-list v-select-list v-sheet theme--light theme--light"]').children().contains(cliente).click()
    }
}

const searchCustomer = (options?: {generalString?: string, struttura?: string, cliente?: string} ) => {
    cy.url().should('equal',clientiPageUrl)
    cy.get('[class="text-body-2 text-sm-h6 font-weight-medium"]').click()
    fillGeneralResearch(options?.generalString)
    selectStructure(options?.struttura)
    selectCliente(options?.cliente)
}

const insertNazione = (nazione?: string) => {
    if(nazione)
        cy.get('label').contains('Nazione').siblings('input').type(nazione)
}

const insertCitta = (citta?: string) => {
    if(citta) 
        cy.get('label').contains('Città').siblings('input').type(citta)
}

const insertIndirizzo = (indirizzo?: string) => {
    if(indirizzo)
        cy.get('label').contains('Indirizzo').siblings('input').type(indirizzo)
}

const insertCodicePostale = (codPostale?: string) => {
    if(codPostale)
        cy.get('label').contains('Codice postale').siblings('input').type(codPostale)
}

const checkSedeCentrale = () => {
    cy.get('[class="v-input--selection-controls__ripple"]').click()
}

const insertDescrizione = (descrizione?: string) => {
    if(descrizione)
        cy.get('label').contains('Descrizione').siblings('textarea').type(descrizione)
}

const confirm = () => {
    cy.get('button').contains('Conferma').click()
    cy.get('[class="v-card__actions"]').children().contains('Conferma').click()
}

/* parte relativa all'aggiunta di Piani (e Aree)    */
const addFloors = (floors?: {numeroPiano: string[], descrizionePiano: string[]}, areas?:{tipologia: string[], descrizioneArea: string[], pianoAssociato: string[]}) => {  
    if(floors && floors.numeroPiano && floors.descrizionePiano){ 
        assert(floors.numeroPiano.length == floors.descrizionePiano.length)
        let length: number = floors.numeroPiano.length
        for (let i = 0; i < length; i++) {
            cy.get('label').contains('Numero piano').siblings('input').type(floors?.numeroPiano[i])
            cy.get('label').contains('Descrizione piano').siblings('textarea').type(floors?.descrizionePiano[i])
            if(areas && areas.tipologia && areas.descrizioneArea && areas.pianoAssociato) {
                assert(areas.tipologia.length == areas.descrizioneArea.length && areas.tipologia.length == areas.pianoAssociato.length)
                cy.get('[class="v-btn__content"]').contains('Aggiungi Aree').click()
                for (let j = 0; j < areas.pianoAssociato.length; j++) {
                    let pianoAssociato = areas.pianoAssociato[j];
                    if(floors.numeroPiano[i] == pianoAssociato) {
                        cy.get('label').contains('Tipologia area').siblings('input').type(areas.tipologia[j])
                        cy.get('label').contains('Descrizione area').siblings('textarea').type(areas.descrizioneArea[j])
                        cy.get('[class="v-btn__content"]').contains('Aggiungi Area').click()
                    }
                }
                cy.get('[class="v-btn v-btn--text theme--light v-size--default"]').last().click({force: true})
            }
            cy.get('[class="v-btn__content"]').contains('Aggiungi piano').click()
        }
    }
}


const addStructureToCustomer = (nazione: string, citta: string, indirizzo: string, codicePostale: string, isCentral: boolean, descrizione: string, options?: {generalString?: string, struttura?: string, cliente?: string}) => {
    searchCustomerAndExpectedTotal(1,options)
    cy.get('[class="v-icon notranslate mr-2 v-icon--link theme--light green--text"]').click()
    insertNazione(nazione)
    insertCitta(citta)
    insertIndirizzo(indirizzo)
    insertCodicePostale(codicePostale)
    if(isCentral)
        checkSedeCentrale()
    insertDescrizione(descrizione)
    confirm()
    cy.wait(500)
}

const addStructureToCustomerWithFloor = (nazione: string, citta: string, indirizzo: string, codicePostale: string, isCentral: boolean, descrizione: string, options?: {generalString?: string, struttura?: string, cliente?: string}, floors?: {numeroPiano: string[], descrizionePiano: string[]}, areas?:{tipologia: string[], descrizioneArea: string[], pianoAssociato: string[]}) => {
    searchCustomerAndExpectedTotal(1,options)
    cy.get('[class="v-icon notranslate mr-2 v-icon--link theme--light green--text"]').click()
    insertNazione(nazione)
    insertCitta(citta)
    insertIndirizzo(indirizzo)
    insertCodicePostale(codicePostale)
    if(isCentral)
        checkSedeCentrale()
    insertDescrizione(descrizione)
    addFloors(floors, areas)
    confirm()
    cy.wait(500)
}

const addSubmit = () => {
    cy.get('button').contains('Conferma').click()
    cy.get('[class="v-card__actions"]').eq(1).contains('Conferma').click()
}

const startNewInsert = () => {
    cy.url().should('equal', clientiPageUrl)
    cy.get('[d="M19,13H13V19H11V13H5V11H11V5H13V11H19V13Z"]').click()
}

const addNome = (nome: string) => {
    cy.get('label').contains('Nome').siblings('input').type(nome)
}

const addEmail = (email: string) => {
    cy.get('label').contains('Email').siblings('input').type(email)
}

const addTelefono = (tel: string) => {
    cy.get('label').contains('Telefono').siblings('input').type(tel)
}

const addCodiceFiscale = (codFisc: string) => {
    cy.get('label').contains('Codice fiscale').siblings('input').type(codFisc)
}

const addPIva = (partitaIva: string) => {
    cy.get('label').contains('Partita Iva').siblings('input').type(partitaIva)
}

const addIndFatt = (indFatt: string) => {
    cy.get('label').contains('Indirizzo fatturazione').siblings('input').type(indFatt)
}

const addDescription = (descrizione: string) => {
    cy.get('label').contains('Descrizione').siblings('textarea').type(descrizione)
}

//every field must be *not null*
const addCustomer = (nome: string, email: string, telefono: string, codFiscale: string, pIva: string, fattIndirizzo: string, description: string) => {
    startNewInsert()
    addNome(nome)
    addEmail(email)
    addTelefono(telefono)
    addCodiceFiscale(codFiscale)
    addPIva(pIva)
    addIndFatt(fattIndirizzo)
    addDescription(description)
    addSubmit()
    cy.wait(500)
}

const searchCustomerAndExpectedTotal = (expectedNum: number, options?: {generalString?: string, struttura?: string, cliente?: string}) => {
    searchCustomer(options)
    if(expectedNum > 10)
        cy.get('[class="v-data-footer__pagination"]').should('contain.text', '1-10 of ' + expectedNum.toString())
    else 
        cy.get('[class="v-data-footer__pagination"]').should('contain.text', '1-'+ expectedNum.toString() + ' of ' + expectedNum.toString())
}

const checkIfStructureIsOwnedByCertainCustomer = (structureName: string, options?: {generalString?: string, struttura?: string, cliente?: string}) => {
    searchCustomerAndExpectedTotal(1,options)
    cy.get('[d="M11.5,18C15.5,18 18.96,15.78 20.74,12.5C18.96,9.22 15.5,7 11.5,7C7.5,7 4.04,9.22 2.26,12.5C4.04,15.78 7.5,18 11.5,18M11.5,6C16.06,6 20,8.65 21.86,12.5C20,16.35 16.06,19 11.5,19C6.94,19 3,16.35 1.14,12.5C3,8.65 6.94,6 11.5,6M11.5,8C14,8 16,10 16,12.5C16,15 14,17 11.5,17C9,17 7,15 7,12.5C7,10 9,8 11.5,8M11.5,9C9.57,9 8,10.57 8,12.5C8,14.43 9.57,16 11.5,16C13.43,16 15,14.43 15,12.5C15,10.57 13.43,9 11.5,9Z"]')
        .click({force: true})

        /**@todo => should be able to extend view to 'All' */
    
    cy.get('[class="text-center"]').contains(structureName)
}

export {
    searchCustomer,
    addCustomer,
    addStructureToCustomer,
    searchCustomerAndExpectedTotal,
    addStructureToCustomerWithFloor,
    checkIfStructureIsOwnedByCertainCustomer
}