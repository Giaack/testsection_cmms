import { getNumberOfClickToReachDate } from "./utils"

const odlPageUrl: string = 'http://localhost:8091/odl'

const openOrCloseLateralMenu = () => {
    cy.url().should('equal', odlPageUrl)
    cy.get('[class="v-btn v-btn--fab v-btn--icon v-btn--round theme--dark v-size--default"]').click({force: true})
}

const insertManutentori = (manutentore?: string) => {
    cy.get('label').contains('Manutentori').siblings('input').click()
    if(manutentore)
        cy.get('[class="v-list v-select-list v-sheet theme--light v-list--dense theme--light"]').children().contains(manutentore).click()
    cy.get('[d="M7,10L12,15L17,10H7Z"]').eq(0).click()
}

const selectResponsabile = (responsabile: string) => {
    cy.get('label').contains('Responsabili').siblings('input').click({force: true}).type(responsabile, {force: true})
    cy.get('[class="v-list v-select-list v-sheet theme--light v-list--dense theme--light"]').children().contains(responsabile).click()
    cy.get('[d="M7,10L12,15L17,10H7Z"]').eq(1).click()
}

const insertDescrizione = (descrizione?: string) => {
    if(descrizione){
        cy.get('label').contains('Descrizione').siblings('[class="v-select__selections"]').click().type(descrizione)
        cy.get('[class="v-list v-select-list v-sheet theme--light v-list--dense theme--light"]').children().contains(descrizione).click()
    }
    cy.get('[d="M7,10L12,15L17,10H7Z"]').eq(2).click()
    
}
//might only be in ('Concluso', 'Abortito', 'Aperto', 'Rifiutata')
const insertStato = (stato?: string) => {
    cy.get('label').contains('Stato').siblings('[class="v-select__selections"]').click()
    if(stato) 
        cy.get('[class="v-list v-select-list v-sheet theme--light v-list--dense theme--light"]').children().contains(stato).click()
    cy.get('[d="M7,10L12,15L17,10H7Z"]').eq(3).click()
}   
/*per ora solamente in ('Ispezione')*/
const selectTipologia = (tipologia?: string) => {
    cy.get('label').contains('Tipologia').siblings('[class="v-select__selections"]').click()
    if(tipologia)
        cy.get('[class="v-list v-select-list v-sheet theme--light v-list--dense theme--light"]').children().contains(tipologia).click()
    cy.get('[d="M7,10L12,15L17,10H7Z"]').eq(4).click()

}
//priorità in ('Bassa', 'Media', 'Alta')
const selectPriorita = (priorita?: string) => {
    cy.get('label').contains('Priorità').siblings('[class="v-select__selections"]').click()
    if(priorita)
        cy.get('[class="v-list v-select-list v-sheet theme--light v-list--dense theme--light"]').children().contains(priorita).click()
    cy.get('[d="M7,10L12,15L17,10H7Z"]').eq(5).click()
}
//pieces for insertNewOdl 
const insertNewDescrizione = (descrizione: string) => {
    cy.get('fieldset').eq(0).siblings().children('input').type(descrizione)
}

const insertPriorita = (priorita: string) => {
    cy.get('fieldset').eq(1).siblings('[class="v-select__slot"]').click()
    cy.get('[class="v-list v-select-list v-sheet theme--light v-list--dense theme--light"]').contains(priorita).click()
}

const selectCategoria = (categoria: string) => {
    cy.get('fieldset').eq(2).siblings('[class="v-select__slot"]').click()
    cy.get('[class="v-list v-select-list v-sheet theme--light v-list--dense theme--light"]').contains(categoria).click()
}

const insertNote = (note: string) => {
    cy.get('textarea').type(note)
}

const insertResponsabile = (responsabile: string) => {
    cy.get('[class="v-select__selection v-select__selection--comma"]').last().click()
    cy.get('[class="v-list-item__title"]').contains('All').click()
    cy.get('[class="v-data-table__wrapper"]').eq(1)
    .children('table').children('tbody').children('tr').children('td').contains(responsabile).siblings('[class="text-start"]').click()
}

const insertAsset = (asset: string) => {
    cy.get('[d="M7.41,8.58L12,13.17L16.59,8.58L18,10L12,16L6,10L7.41,8.58Z"]').last().click({force: true})
    cy.get('[class="v-select__selection v-select__selection--comma"]').last().click()
    cy.get('[role="option"]').last().click({force: true})
    cy.get('[class="v-data-table__wrapper"]').eq(2)
    .children('table').children('tbody').children('tr').children('td').contains(asset).siblings('[class="text-start"]').click()
}

const confirm = () => {
    cy.get('[class="v-btn v-btn--text theme--light v-size--default"]').contains('Accetta').click()
    cy.get('[class="v-btn v-btn--text theme--light v-size--default"]').contains('Conferma').click()
}

const openGestioneOdl = (options?: {manutentore?: string, responsabile?: string, descrizione?: string, stato?: string, tipologia?: string, priorita?: string}) => {
    searchOdlAndExpectedTotal(1,options)
    cy.get('[d="M15.5,12C18,12 20,14 20,16.5C20,17.38 19.75,18.21 19.31,18.9L22.39,22L21,23.39L17.88,20.32C17.19,20.75 16.37,21 15.5,21C13,21 11,19 11,16.5C11,14 13,12 15.5,12M15.5,14A2.5,2.5 0 0,0 13,16.5A2.5,2.5 0 0,0 15.5,19A2.5,2.5 0 0,0 18,16.5A2.5,2.5 0 0,0 15.5,14M5,3H19C20.11,3 21,3.89 21,5V13.03C20.5,12.23 19.81,11.54 19,11V5H5V19H9.5C9.81,19.75 10.26,20.42 10.81,21H5C3.89,21 3,20.11 3,19V5C3,3.89 3.89,3 5,3M7,7H17V9H7V7M7,11H12.03C11.23,11.5 10.54,12.19 10,13H7V11M7,15H9.17C9.06,15.5 9,16 9,16.5V17H7V15Z"]')
        .click({force: true})
    // la gestione-odl viene aperta
}

const openResponsabileTab = (options?: {manutentore?: string, responsabile?: string, descrizione?: string, stato?: string, tipologia?: string, priorita?: string}) => {
    openGestioneOdl(options)
    // cy.get('[role="tab"]').contains('Responsabile').click()
}

const openPersonaleTab = (options?: {manutentore?: string, responsabile?: string, descrizione?: string, stato?: string, tipologia?: string, priorita?: string}) => {
    openGestioneOdl(options)
    cy.get('[role="tab"]').contains('Personale').click()
}

const selectManutentore = (cognome: string)  => {
    //eq(3) si riferisce alla selezione di quanti elementi visualizzare per la prima tabella che si 'vede' in Associate new Maintainer
    cy.get('[class="v-select__selection v-select__selection--comma"]').eq(3).click()
    cy.get('[class="v-list-item__title"]').contains('All').click()
    cy.get('[class="text-left"]').contains(cognome).siblings('[class="text-start"]').click()   
    cy.get('[class="v-btn__content"]').contains('Associa selezioni').click() 
}

/** permette di inserire la data di inizio e fine previste, la durata prevista e l'orario di inizio */
const insertTimeInfos = (startDate?: string, conclusionDate?: string, prevDuration?: string, startHour?: string) => {
    if(startDate){        
        cy.get('label').contains('Inizio previsto').siblings().click()
        let numberOfClicks4Start = getNumberOfClickToReachDate(startDate)
        for (let i = 0; i < numberOfClicks4Start; i++) {
            cy.get('[d="M8.59,16.58L13.17,12L8.59,7.41L10,6L16,12L10,18L8.59,16.58Z"]').last().click({force: true})
        }
        let dayOfStart = parseInt(startDate.split('/')[0]).toString()
        cy.get('[class="v-btn__content"]').contains(dayOfStart).click()
        cy.get('[class="v-btn__content"]').contains('Ok').click()
    }
    let prevDurationHour = prevDuration?.split(':')[0]
    let prevDurationMinutes = prevDuration?.split(':')[1]
    let startHourHour = startHour?.split(':')[0]
    let startHourMinutes = startHour?.split(':')[1]
    // if(conclusionDate) {
    //     let numberOfClicks4Conclusion = getNumberOfClickToReachDate(conclusionDate)
    //     let dayOfConclusion = parseInt(conclusionDate.split('/')[0]).toString()
    //     cy.get('label').contains('Fine prevista').siblings().click()
    //     for (let i = 0; i < numberOfClicks4Conclusion; i++) {
    //         cy.get('[d="M8.59,16.58L13.17,12L8.59,7.41L10,6L16,12L10,18L8.59,16.58Z"]').last().click({force: true})
    //     }
    //     cy.get('[class="v-menu__content theme--light v-menu__content--fixed menuable__content__active"]').children().contains(dayOfConclusion).click()
    //     cy.get('[class="v-menu__content theme--light v-menu__content--fixed menuable__content__active"]').children().contains('Ok').click()
    // }
    if(prevDurationHour && prevDurationMinutes) {
        cy.get('label').contains('Durata prevista').siblings().click()
        cy.get('[class="v-time-picker-clock v-time-picker-clock--indeterminate theme--light"]').children().contains(prevDurationHour).click()
        cy.get('[class="v-time-picker-clock v-time-picker-clock--indeterminate theme--light"]').children().contains(prevDurationMinutes).click()
        cy.get('[class="v-picker v-card v-picker--time theme--light"]').children().contains('Ok').click()
    }
    if(startHourHour && startHourMinutes) {
        cy.get('label').contains('Orario inizio previsto').siblings().click()
        cy.get('[class="v-time-picker-clock v-time-picker-clock--indeterminate theme--light"]').children().contains(startHourHour).click()
        cy.get('[class="v-time-picker-clock v-time-picker-clock--indeterminate theme--light"]').children().contains(startHourMinutes).click()
        cy.get('[class="v-picker__actions v-card__actions"]').last().children().contains('Ok').click()
    }
    cy.get('[class="v-expansion-panel-header px-0 py-2 pr-4 v-expansion-panel-header--active"]').eq(0).click()
}

const checkManutentoreInResponsabili = (codFisc: string, options?: {manutentore?: string, responsabile?: string, descrizione?: string, stato?: string, tipologia?: string, priorita?: string}) => {
    openPersonaleTab({descrizione: options?.descrizione})
    //aggiungere l'opzione per estendere le righe 'visibili' ad All
    cy.get('[class="text-left"]').contains(codFisc)
}

/** la funzione permette di effettuare la ricerca di un odl all'interno della pagina odl, tramite dei parametri opzionali che
andranno a rimepire i campi di ricerca */
const searchOdl = (options?: {manutentore?: string, responsabile?: string, descrizione?: string, stato?: string, tipologia?: string, priorita?: string}) => {
    cy.url().should('equal', odlPageUrl)
    cy.get('[class="text-body-2 text-sm-h6 font-weight-medium"]').click()
    if(options?.manutentore)
        insertManutentori(options?.manutentore)
    if(options?.responsabile)
        selectResponsabile(options.responsabile)
    cy.wait(750)
    if(options?.descrizione)
        insertDescrizione(options?.descrizione)
    if(options?.stato)
        insertStato(options?.stato)
    if(options?.tipologia)
        selectTipologia(options?.tipologia)
    if(options?.priorita)
        selectPriorita(options?.priorita)
    cy.get('[class="text-body-2 text-sm-h6 font-weight-medium"]').click()
}
/**  * è una funzione che chiama la searchOdl e controlla, che il numero di risultati nella pagina odl sia uguale ad expectedNum */
const searchOdlAndExpectedTotal = (expectedNum: number, options?: {manutentore?: string, responsabile?: string, descrizione?: string, stato?: string, tipologia?: string, priorita?: string}) => {
    searchOdl(options)
    if(expectedNum > 10)
        cy.get('[class="v-data-footer__pagination"]').should('contain.text', '1-10 of ' + expectedNum.toString())
    else 
        cy.get('[class="v-data-footer__pagination"]').should('contain.text', '1-'+ expectedNum.toString() + ' of ' + expectedNum.toString())
    cy.wait(500)
}   

const insertNewOdl = (descrizione: string, priorita: string, categoria: string, responsabile: string, asset:string, note?: string, timeOptions?:{startDate?: string, conclusionDate?: string, prevDuration?: string, startHour?: string}) => {
    cy.url().should('equal', odlPageUrl)
    cy.get('[d="M19,13H13V19H11V13H5V11H11V5H13V11H19V13Z"]').click()
    insertNewDescrizione(descrizione)
    insertPriorita(priorita)
    selectCategoria(categoria)
    if(note)    
        insertNote(note)
    if(timeOptions)
        insertTimeInfos(timeOptions.startDate!, timeOptions.conclusionDate!, timeOptions.prevDuration!, timeOptions.startHour!)      
    insertResponsabile(responsabile)
    insertAsset(asset)
    confirm()
}   

/**  newState in ('Da accettare' -> Accettata' | 'Rifiutata') oppure in ('Aperto' -> Aperto' | 'In lavorazione' | 'Bloccato' | 'Concluso' | 'Abortito')*/
const changeOdlRequestState = (newState: string, options?: {manutentore?: string,  responsabile?: string, descrizione?: string, stato?: string, tipologia?: string, priorita?: string}) => {
    //this function is thougth to accept a single request, so the search has to be really satisfying
    searchOdlAndExpectedTotal(1,options)        // -> as a breakpoint so that if the search doesn't show an only result we dont' continue the test
    cy.get('[d="M12,2C6.47,2 2,6.47 2,12C2,17.53 6.47,22 12,22C17.53,22 22,17.53 22,12C22,6.47 17.53,2 12,2M15.1,7.07C15.24,7.07 15.38,7.12 15.5,7.23L16.77,8.5C17,8.72 17,9.07 16.77,9.28L15.77,10.28L13.72,8.23L14.72,7.23C14.82,7.12 14.96,7.07 15.1,7.07M13.13,8.81L15.19,10.87L9.13,16.93H7.07V14.87L13.13,8.81Z"]')
        .eq(0).click({force: true})
    //il test non fa differenza da 'Aperto' a 'In lavorazione', sempmlicemente seleziona il primo elemento cliccabile dopo la ricerca
    cy.get('label').contains(newState).siblings().children('input').click({force:true})
    cy.get('[class="v-btn__content"]').contains('Cambia').click()
    cy.get('[class="v-btn__content"]').contains('Conferma').click()
}

const changeOdlRequestStateWithCheck = (oldState: string, newState: string, options?: {manutentore?: string, responsabile?: string, descrizione?: string, stato?: string, tipologia?: string, priorita?: string}) => {
    //this function is thougth to accept a single request, so the search has to be really satisfying
    searchOdlAndExpectedTotal(1,options)        // -> as a breakpoint so that if the search doesn't show an only result we dont' continue the test
    cy.get('[d="M12,2C6.47,2 2,6.47 2,12C2,17.53 6.47,22 12,22C17.53,22 22,17.53 22,12C22,6.47 17.53,2 12,2M15.1,7.07C15.24,7.07 15.38,7.12 15.5,7.23L16.77,8.5C17,8.72 17,9.07 16.77,9.28L15.77,10.28L13.72,8.23L14.72,7.23C14.82,7.12 14.96,7.07 15.1,7.07M13.13,8.81L15.19,10.87L9.13,16.93H7.07V14.87L13.13,8.81Z"]')
        .eq(0).parent().parent().parent().contains(oldState).click({force: true})
    //il test non fa differenza da 'Aperto' a 'In lavorazione', sempmlicemente seleziona il primo elemento cliccabile dopo la ricerca
    cy.get('label').contains(newState).siblings().children('input').click({force:true})
    cy.get('[class="v-btn__content"]').contains('Cambia').click()
    cy.get('[class="v-btn__content"]').contains('Conferma').click()
}

/** the function works only for name or surname, beacuse it checks the responsabile also in the main odl screen */
const checkResponsabileOfOdl = (expectedResponsabile: string, options?: {manutentore?: string, responsabile?: string, descrizione?: string, stato?: string, tipologia?: string, priorita?: string}) => {
    searchOdlAndExpectedTotal(1,{descrizione: options?.descrizione})
    cy.get('[class="text-left"]').eq(15).should('contain.text', expectedResponsabile)
    cy.reload()
    openResponsabileTab({descrizione: options?.descrizione})
    cy.get('td').contains(expectedResponsabile)
}

const checkPersonaleOfOdl = (expectedPersonale: string, options?: {manutentore?: string, responsabile?: string, descrizione?: string, stato?: string, tipologia?: string, priorita?: string}) => {
    openPersonaleTab({descrizione: options?.descrizione})
    cy.get('[class="text-left"]').contains(expectedPersonale)
}

const addPersonaleToOdl = (cognome: string, options?: {manutentore?: string, responsabile?: string, descrizione?: string, stato?: string, tipologia?: string, priorita?: string}) => {
    openPersonaleTab({descrizione: options?.descrizione})
    cy.get('[class="v-btn__content"]').contains('Nuovo').click()
    selectManutentore(cognome)
}

export {
    openOrCloseLateralMenu,
    searchOdl,
    searchOdlAndExpectedTotal,
    insertNewOdl,
    changeOdlRequestState,
    changeOdlRequestStateWithCheck,
    openGestioneOdl, //
    openResponsabileTab, //
    checkResponsabileOfOdl,
    addPersonaleToOdl,
    checkManutentoreInResponsabili,
    checkPersonaleOfOdl
}

