import { fromPairs } from "cypress/types/lodash"
import { checkNumberOfUserInResearch } from "./usersPage"

const manutentoriPageUrl: string = 'http://localhost:8091/maintainers'

const insertGeneralSearch = (generalSearch: string) => {
    cy.get('label').contains('Ricerca generale').siblings('input').type(generalSearch)
}

const insertRuolo = (ruolo: string) => {
    cy.get('label').contains('Cerca per ruolo').siblings('input').click()
    cy.get('[class="v-list v-select-list v-sheet theme--light v-list--dense theme--light"]').children().contains(ruolo).click()
}

const insertSkill = (skill: string) => {
    cy.get('label').contains('Cerca per skill').siblings('input').click()
    cy.get('[class="v-list v-select-list v-sheet theme--light v-list--dense theme--light"]').children().contains(skill).click()
}

const searchManutentore = (options?:{generalSearch?: string, ruolo?: string, skill?: string, attivi?: boolean}) => {
    cy.url().should('equal', manutentoriPageUrl)
    cy.get('[class="text-body-2 text-sm-h6 font-weight-medium"]').click()   //apro il menù di ricerca
    if(options?.generalSearch)
        insertGeneralSearch(options.generalSearch)
    if(options?.ruolo)
        insertRuolo(options.ruolo)
    if(options?.skill)
        insertSkill(options.skill)
    if(!options?.attivi) {
        cy.get('[class="v-input--selection-controls__ripple orange--text text--darken-3"]').siblings('input').click({force: true})
    }
}

const insertNomeToChange = (nome: string) => {
    cy.get('label').contains('Nome').siblings('input').clear().type(nome)
}

const insertCognomeToChange = (cognome: string) => {
    cy.get('label').contains('Cognome').siblings('input').clear().type(cognome)
}

const insertResidenzaToChange = (residenza: string) => {
    cy.get('label').contains('Residenza').siblings('input').clear().type(residenza)
}

const insertCodFiscToChange = (codFisc: string) => {
    cy.get('label').contains('Codice fiscale').siblings('input').clear().type(codFisc)
}

const insertEmailToChange = (email: string) => {
    cy.get('label').contains('Mail').siblings('input').clear().type(email)
}

const insertTelefonoToChange = (tel: string) => {
    cy.get('label').contains('Recapito telefonico').siblings('input').clear().type(tel)
}

const searchManutentoreAndExpectTotal = (expectedNum: number , options?:{generalSearch?: string, ruolo?: string, skill?: string, attivi?: boolean}) => {
    searchManutentore(options)
    if(expectedNum > 10)
        cy.get('[class="v-data-footer__pagination"]').should('contain.text', '1-10 of ' + expectedNum.toString())
    else if (expectedNum == 0)
        cy.get('[class="v-data-footer__pagination"]').invoke('text').should('eq', '–')
    else 
        cy.get('[class="v-data-footer__pagination"]').should('contain.text', '1-'+ expectedNum.toString() + ' of ' + expectedNum.toString())
}

const confirmChange = () => {
    cy.get('button').contains('Modifica').click()
    cy.get('button').contains('Conferma').click()
}

const modifyMantainer = (changes?: {nome?: string, cognome?: string, residenza?: string, codFiscale?: string, email?: string, telefono?: string}, options?: {generalSearch?: string, ruolo?: string, skill?: string, attivi?: boolean}) => {
    searchManutentoreAndExpectTotal(1, options) //se la ricerca non restituisce un solo risultato non va bene
    cy.get('[d="M19.71,8.04L17.37,10.37L13.62,6.62L15.96,4.29C16.35,3.9 17,3.9 17.37,4.29L19.71,6.63C20.1,7 20.1,7.65 19.71,8.04M3,17.25L13.06,7.18L16.81,10.93L6.75,21H3V17.25M16.62,5.04L15.08,6.58L17.42,8.92L18.96,7.38L16.62,5.04M15.36,11L13,8.64L4,17.66V20H6.34L15.36,11Z"]')
        .click({force: true})
    if(changes?.nome)
        insertNomeToChange(changes.nome)
    if((changes?.cognome))
        insertCognomeToChange(changes.cognome)
    if(changes?.residenza)
        insertResidenzaToChange(changes.residenza)
    if(changes?.codFiscale)
        insertCodFiscToChange(changes.codFiscale)
    if(changes?.email)
        insertEmailToChange(changes.email)
    if(changes?.telefono)
        insertTelefonoToChange(changes.telefono)
    confirmChange()
}

/** lista di funzioni mock x la modifyMaintainerNull */
const insertNomeToChangeNull = (nome: string) => {
    cy.get('label').contains('Nome').siblings('input').clear()
}

const insertCognomeToChangeNull = (cognome: string) => {
    cy.get('label').contains('Cognome').siblings('input').clear()
}

const insertResidenzaToChangeNull = (residenza: string) => {
    cy.get('label').contains('Residenza').siblings('input').clear()
}

const insertCodFiscToChangeNull = (codFisc: string) => {
    cy.get('label').contains('Codice fiscale').siblings('input').clear()
}

const insertEmailToChangeNull = (email: string) => {
    cy.get('label').contains('Mail').siblings('input').clear()
}

const insertTelefonoToChangeNull = (tel: string) => {
    cy.get('label').contains('Recapito telefonico').siblings('input').clear()
}


/** funzione mock, usata per inserire stringhe vuote nei campi della modifica del maintainer */
const modifyMantainerNull = (nome: string, cognome: string, residenza: string, email: string, telefono: string, options?: {generalSearch?: string, ruolo?: string, skill?: string, attivi?: boolean}) => {
    searchManutentoreAndExpectTotal(1, options) //se la ricerca non restituisce un solo risultato non va bene
    cy.get('[d="M19.71,8.04L17.37,10.37L13.62,6.62L15.96,4.29C16.35,3.9 17,3.9 17.37,4.29L19.71,6.63C20.1,7 20.1,7.65 19.71,8.04M3,17.25L13.06,7.18L16.81,10.93L6.75,21H3V17.25M16.62,5.04L15.08,6.58L17.42,8.92L18.96,7.38L16.62,5.04M15.36,11L13,8.64L4,17.66V20H6.34L15.36,11Z"]')
        .click({force: true})
    insertNomeToChangeNull(nome)
    insertCognomeToChangeNull(cognome)
    insertResidenzaToChangeNull(residenza)
    insertEmailToChangeNull(email)
    insertTelefonoToChangeNull(telefono)
    confirmChange()
}

const openSkillsRolesTabOfMaintainer = (options?:{generalSearch?: string, ruolo?: string, skill?: string, attivi?: boolean}) => {
    searchManutentoreAndExpectTotal(1,options)
    cy.get('[d="M11.5,18C15.5,18 18.96,15.78 20.74,12.5C18.96,9.22 15.5,7 11.5,7C7.5,7 4.04,9.22 2.26,12.5C4.04,15.78 7.5,18 11.5,18M11.5,6C16.06,6 20,8.65 21.86,12.5C20,16.35 16.06,19 11.5,19C6.94,19 3,16.35 1.14,12.5C3,8.65 6.94,6 11.5,6M11.5,8C14,8 16,10 16,12.5C16,15 14,17 11.5,17C9,17 7,15 7,12.5C7,10 9,8 11.5,8M11.5,9C9.57,9 8,10.57 8,12.5C8,14.43 9.57,16 11.5,16C13.43,16 15,14.43 15,12.5C15,10.57 13.43,9 11.5,9Z"]').eq(0)
        .click({force: true})
    // siamo arrivati alla pagina di ruoli e skills del manutentore
}

const checkIfRoleAssociatedToMaintainer = (roleToCheck: string[], options?:{generalSearch?: string, ruolo?: string, skill?: string, attivi?: boolean}) => {
    openSkillsRolesTabOfMaintainer(options)
    roleToCheck.forEach(role => {
        cy.get('[class="text-left"]').contains(role)    
    });
}

const checkIfSkillAssociatedToMaintainer = (skillToCheck: string[], options?:{generalSearch?: string, ruolo?: string, skill?: string, attivi?: boolean}) => {
    openSkillsRolesTabOfMaintainer(options)
    skillToCheck.forEach(skill => {
        cy.get('[class="text-left"]').contains(skill)    
    });
}

const addSkillToMaintainer = (newSkill: string, options?:{generalSearch?: string, ruolo?: string, skill?: string, attivi?: boolean}) => {
    openSkillsRolesTabOfMaintainer(options)
    cy.get('[role="tab"]').eq(4).click()
    cy.get('[d="M7,10L12,15L17,10H7Z"]').eq(5).click({force:true})
    cy.get('[class="v-list v-select-list v-sheet theme--light theme--light"]').last().children().contains('All').click({force: true})
    cy.get('[class="text-left"]').contains(newSkill).siblings('[class="text-start"]').click()
    cy.get('[d="M21,7L9,19L3.5,13.5L4.91,12.09L9,16.17L19.59,5.59L21,7Z"]').click({force: true})
    cy.get('[class="v-card__actions"]').children('button').children().contains('Conferma').click()
}

const addRoleToMaintainer = (newRole: string, options?:{generalSearch?: string, ruolo?: string, skill?: string, attivi?: boolean}) => {
    openSkillsRolesTabOfMaintainer(options)
    cy.get('[role="tab"]').eq(1).click()
    cy.get('[d="M7,10L12,15L17,10H7Z"]').eq(4).click({force:true})
    cy.get('[class="v-list v-select-list v-sheet theme--light theme--light"]').last().children().contains('All').click({force: true})
    cy.get('[class="text-left"]').contains(newRole).siblings('[class="text-start"]').click()
    cy.get('[d="M21,7L9,19L3.5,13.5L4.91,12.09L9,16.17L19.59,5.59L21,7Z"]').click({force: true})
    cy.get('[class="v-card__actions"]').children('button').children().contains('Conferma').click()
}

const addManutentore = (name: string, surname: string, codiceFiscale: string, email: string, options?: {residenza?: string, telefono?: string, ruolo?: string, abilita?: string}) => {
    cy.url().should('equal', manutentoriPageUrl)
    cy.get('[d="M19,13H13V19H11V13H5V11H11V5H13V11H19V13Z"]').click({force: true}).then(() => {
        cy.get('label').contains('Nome').siblings().click({force: true}).type(name)
        cy.get('label').contains('Cognome',).siblings().click({force: true}).type(surname)
        if(options?.residenza)
            cy.get('label').contains('Residenza').siblings().click({force: true}).type(options.residenza)
        cy.get('label').contains('Codice fiscale').siblings().click({force: true}).type(codiceFiscale)
        cy.get('label').contains('Mail').siblings().click({force: true}).type(email)
        if(options?.telefono)
            cy.get('label').contains('Recapito telefonico').siblings().click({force: true}).type(options.telefono)
        if(options?.ruolo){
            cy.get('label').contains('Ruoli').click({force: true})
            cy.get('[class="v-list-item v-list-item--link theme--light"]').contains(options.ruolo).click()
        }
        if(options?.abilita){
            cy.get('label').contains('Abilità').click({force: true})
            cy.get('[class="v-list-item v-list-item--link theme--light"]').contains(options.abilita).click()
        }
            cy.get('[class="v-btn__content"]').contains('Conferma').click()
        cy.get('[class="v-btn v-btn--text theme--light v-size--default"]').eq(2).click()
        cy.wait(500)                
    })
}

const deactivateManutentore = (manutentore: string) => {
    searchManutentoreAndExpectTotal(1, {generalSearch: manutentore})
    cy.get('[d="M19.71,8.04L17.37,10.37L13.62,6.62L15.96,4.29C16.35,3.9 17,3.9 17.37,4.29L19.71,6.63C20.1,7 20.1,7.65 19.71,8.04M3,17.25L13.06,7.18L16.81,10.93L6.75,21H3V17.25M16.62,5.04L15.08,6.58L17.42,8.92L18.96,7.38L16.62,5.04M15.36,11L13,8.64L4,17.66V20H6.34L15.36,11Z"]')
        .click({force: true})
    cy.get('[role="switch"]').last().click({force: true})
    confirmChange()
}

export {
    searchManutentore,
    searchManutentoreAndExpectTotal,
    modifyMantainer,
    addRoleToMaintainer,
    addSkillToMaintainer,
    checkIfSkillAssociatedToMaintainer,
    checkIfRoleAssociatedToMaintainer,
    modifyMantainerNull,
    addManutentore,
    deactivateManutentore
}