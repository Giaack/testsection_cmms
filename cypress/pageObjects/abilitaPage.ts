const skillsPageUrl: string = 'http://localhost:8091/skills'

const insertRicercaGenerale = (ricerca: string) => {
    cy.get('label').contains('Ricerca generale').siblings('input').type(ricerca)
}

const insertManutentore = (manutentore: string) => {
    cy.get('label').contains('Cerca per manutentore').siblings('[type="text"]').click()
    cy.get('[class="v-list v-select-list v-sheet theme--light theme--light"]').children().contains(manutentore).click()
    //cy.get('[d="M7,10L12,15L17,10H7Z"]').eq(0).click()    => in this case it automatically close the menu a tendina
}

const checkOnlyActive = () => {
    cy.get('[role="switch"]').click()
}

const insertTipo = (tipo: string) => {
    cy.get('label').contains('Tipo').siblings('input').clear().type(tipo)
}

const insertDescrizione = (desc: string) => {
    cy.get('label').contains('Descrizione').siblings('textarea').clear().type(desc)
}

const insertDescrizioneNull = () => {
    cy.get('label').contains('Descrizione').siblings('textarea').clear()
}

const insertAssociati = (associati: string[]) => {
    associati.forEach(associato => {
        cy.get('label').contains('Associa a').siblings('[class="v-input__append-inner"]').children().click().type(associato)
        cy.get('[class="v-menu__content theme--light v-menu__content--fixed menuable__content__active v-autocomplete__content"]').children().contains(associato).click()    
        cy.get('label').contains('Associa a').siblings('[class="v-input__append-inner"]').children().click()
    });   
}

const confirmAdd = () => {
    cy.get('[class="v-btn__content"]').contains('Conferma').click()
    cy.get('[class="v-card__actions"]').last().contains('Conferma').click()
}

const confirmModify = () => {
    cy.get('[class="v-btn__content"]').contains('Modifica').click()
    cy.get('[class="v-btn__content"]').contains('Conferma').click()
}

const insertTipoModify = (tipo: string) => {
    cy.get('label').contains('Tipo').siblings('textarea').clear().type(tipo)
}

/* ************************************* */

const addSkill = (tipo: string, descrizione: string, associati?: string[]) => {
    cy.url().should('equal', skillsPageUrl)
    cy.get('[d="M19,13H13V19H11V13H5V11H11V5H13V11H19V13Z"]').click()
    insertTipo(tipo)
    insertDescrizione(descrizione)
    if(associati && associati.length > 0)
        insertAssociati(associati)
    confirmAdd()
}

const searchSkill = (options?: {generalSearch?: string, manutentore?: string, onlyActive?: boolean}) => {
    cy.url().should('equal', skillsPageUrl)
    cy.get('[class="text-body-2 text-sm-h6 font-weight-medium"]').click()   //apro il menù di ricerca
    if(options?.generalSearch)
        insertRicercaGenerale(options.generalSearch)
    if(options?.manutentore)
        insertManutentore(options.manutentore)
    if(options?.onlyActive)
        checkOnlyActive()
}

const searchSkillWithExpectedNum = (expectedNum: number, options?: {generalSearch?: string, manutentore?: string, onlyActive?: boolean}) => {
    searchSkill(options)
    if(expectedNum > 10)
        cy.get('[class="v-data-footer__pagination"]').should('contain.text', '1-10 of ' + expectedNum.toString())
    else 
        cy.get('[class="v-data-footer__pagination"]').should('contain.text', '1-'+ expectedNum.toString() + ' of ' + expectedNum.toString())
}

const deactivateSkill = (options?: {generalSearch?: string, manutentore?: string, onlyActive?: boolean}, descrizione?: string) => {
    searchSkillWithExpectedNum(1,options)     //solo se il ruolo cercato è 'unico'
    cy.get('[d="M19,4H15.5L14.5,3H9.5L8.5,4H5V6H19M6,19A2,2 0 0,0 8,21H16A2,2 0 0,0 18,19V7H6V19Z"]').click()
    cy.get('[class="v-btn__content"]').contains('Conferma').click()
}

const modifySkill = (tipo?: string, descrizione?: string, options?: {generalSearch?: string, manutentore?: string, onlyActive?: boolean} ) => {
    searchSkillWithExpectedNum(1, options)
    cy.get('[d="M19.71,8.04L17.37,10.37L13.62,6.62L15.96,4.29C16.35,3.9 17,3.9 17.37,4.29L19.71,6.63C20.1,7 20.1,7.65 19.71,8.04M3,17.25L13.06,7.18L16.81,10.93L6.75,21H3V17.25M16.62,5.04L15.08,6.58L17.42,8.92L18.96,7.38L16.62,5.04M15.36,11L13,8.64L4,17.66V20H6.34L15.36,11Z"]')
        .click({force: true})
    if(tipo)
        insertTipoModify(tipo)
    if(descrizione)
        insertDescrizione(descrizione)
    confirmModify()
}

/** mock function to insert a null descrizione (note) */
const modifySkillNull = (tipo?: string, options?: {generalSearch?: string, manutentore?: string, onlyActive?: boolean} ) => {
    searchSkillWithExpectedNum(1, options)
    cy.get('[d="M19.71,8.04L17.37,10.37L13.62,6.62L15.96,4.29C16.35,3.9 17,3.9 17.37,4.29L19.71,6.63C20.1,7 20.1,7.65 19.71,8.04M3,17.25L13.06,7.18L16.81,10.93L6.75,21H3V17.25M16.62,5.04L15.08,6.58L17.42,8.92L18.96,7.38L16.62,5.04M15.36,11L13,8.64L4,17.66V20H6.34L15.36,11Z"]')
        .click({force: true})
    if(tipo)
        insertTipoModify(tipo)
    insertDescrizioneNull()
    confirmModify()
}

export {
    addSkill,
    searchSkill,
    searchSkillWithExpectedNum,
    modifySkill,
    deactivateSkill,
    modifySkillNull
}