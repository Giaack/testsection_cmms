const plannedMaintenancePageUrl: string = 'http://localhost:8091/plannedMaintenance'
import { getNumberOfClickToReachDate} from "./utils"

const openOrCloseLateralMenu = () => {
    cy.url().should('equal', plannedMaintenancePageUrl)
    cy.get('[class="v-btn v-btn--fab v-btn--icon v-btn--round theme--dark v-size--default"]').click({force: true})
}

const startAddingNewPlannedMaintenance = () => {
    cy.url().should('equal', plannedMaintenancePageUrl)
    cy.get('[d="M19,13H13V19H11V13H5V11H11V5H13V11H19V13Z"]').click()
}

// inserisce i dati in generalità e poi chiude il riquadro
const insertGeneralita = (description: string, priority: priorities) => {
    cy.get('textarea').type(description)
    cy.get('[d="M7,10L12,15L17,10H7Z"]').eq(1).click()
    cy.get('[class="v-list-item__title"]').contains('Bassa').click()
    cy.get('[class="v-expansion-panel-header px-0 py-2 pr-4 v-expansion-panel-header--active"]').eq(0).click()
}

// le date startDate e conclusionDate devono essere inserite come stringhe che rispettino il formato 'dd/mm/yyyy'
const insertTimeInfos = (startDate: string, conclusionDate?: string, prevDuration?: string, startHour?: string) => {
    cy.get('label').contains('Inizio').siblings().click()
    let numberOfClicks4Start = getNumberOfClickToReachDate(startDate)
    for (let i = 0; i < numberOfClicks4Start; i++) {
        cy.get('[d="M8.59,16.58L13.17,12L8.59,7.41L10,6L16,12L10,18L8.59,16.58Z"]').last().click({force: true})
    }
    let dayOfStart = parseInt(startDate.split('/')[0]).toString()
    cy.get('[class="v-btn__content"]').contains(dayOfStart).click()
    cy.get('[class="v-btn__content"]').contains('Ok').click()
    let prevDurationHour = prevDuration?.split(':')[0]
    let prevDurationMinutes = prevDuration?.split(':')[1]
    let startHourHour = startHour?.split(':')[0]
    let startHourMinutes = startHour?.split(':')[1]
    if(conclusionDate) {
        let numberOfClicks4Conclusion = getNumberOfClickToReachDate(conclusionDate)
        let dayOfConclusion = parseInt(conclusionDate.split('/')[0]).toString()
        cy.get('label').contains('Conclusione').siblings().click()
        for (let i = 0; i < numberOfClicks4Conclusion; i++) {
            cy.get('[d="M8.59,16.58L13.17,12L8.59,7.41L10,6L16,12L10,18L8.59,16.58Z"]').last().click({force: true})
        }
        cy.get('[class="v-menu__content theme--light v-menu__content--fixed menuable__content__active"]').children().contains(dayOfConclusion).click()
        cy.get('[class="v-menu__content theme--light v-menu__content--fixed menuable__content__active"]').children().contains('Ok').click()
    }
    if(prevDurationHour && prevDurationMinutes) {
        cy.get('label').contains('Durata prevista').siblings().click()
        cy.get('[class="v-time-picker-clock v-time-picker-clock--indeterminate theme--light"]').children().contains(prevDurationHour).click()
        cy.get('[class="v-time-picker-clock v-time-picker-clock--indeterminate theme--light"]').children().contains(prevDurationMinutes).click()
        cy.get('[class="v-picker v-card v-picker--time theme--light"]').children().contains('Ok').click()
    }
    if(startHourHour && startHourMinutes) {
        cy.get('label').contains('Orario inizio').siblings().click()
        cy.get('[class="v-time-picker-clock v-time-picker-clock--indeterminate theme--light"]').children().contains(startHourHour).click()
        cy.get('[class="v-time-picker-clock v-time-picker-clock--indeterminate theme--light"]').children().contains(startHourMinutes).click()
        cy.get('[class="v-picker__actions v-card__actions"]').last().children().contains('Ok').click()
    }
    cy.get('[class="v-expansion-panel-header px-0 py-2 pr-4 v-expansion-panel-header--active"]').eq(0).click()
}

const insertGestioneAsset = (assetToSearch: string) => {
    cy.get('[class="v-expansion-panel-header px-0 py-2 pr-4"]').children().contains('Gestione asset').click().then(() => {
        cy.get('[class="v-select__selection v-select__selection--comma"]').last().click()
        cy.get('[class="v-list-item__title"]').contains('All').click()
        cy.get('[class="v-expansion-panel-header px-0 py-2 pr-4 v-expansion-panel-header--active"]').siblings('[class="v-expansion-panel-content"]')
        .children().children().children().children().children('[class="v-data-table__wrapper"]').children('table').children('tbody').children('tr').children('td').contains(assetToSearch).siblings('[class="text-start"]').click()
    })
    cy.get('[class="v-expansion-panel-header px-0 py-2 pr-4 v-expansion-panel-header--active"]').eq(0).click()
}

const insertResponsabile = (responsabile: string) => {
    cy.get('[class="v-expansion-panel-header px-0 py-2 pr-4"]').children().contains('Manutentori').click().then(() => {
        cy.get('[class="v-select__selection v-select__selection--comma"]').last().click()
        cy.get('[role="option"]').last().click({force: true})
        cy.get('[class="v-expansion-panel-header px-0 py-2 pr-4 v-expansion-panel-header--active"]').siblings()
        .children().children().children('[class="v-data-table__wrapper"]').children('table').children('tbody').children('tr').children('td').contains(responsabile).siblings('[class="text-start"]').click()
    })
    cy.get('[class="v-expansion-panel-header px-0 py-2 pr-4 v-expansion-panel-header--active"]').eq(0).click()
}

const chooseFrequenza = (tipo: string, frequenza: string, floating?: boolean) => {
    cy.get('[class="v-expansion-panel-header px-0 py-2 pr-4"]').children().contains('Frequenza').click().then(() => {
        cy.get('[class="v-input--radio-group__input"]').children().contains('Standard')
       // cy.get('[class="v-input--radio-group__input"]').children().contains('Custom').click()
    })
    cy.get('label').contains('Tipo frequenza').siblings().children().eq(0).click().then(() => {
        cy.get('[class="v-menu__content theme--light v-menu__content--fixed menuable__content__active"]').contains(tipo).click()
    })
    cy.get('label').contains('Frequenza').siblings().children().eq(0).click().then(() => {
        cy.get('[class="v-menu__content theme--light v-menu__content--fixed menuable__content__active"]').contains('1').click()
    })
    
    if(floating) {
        cy.get('[type="checkbox"]').click({force: true})    //usiamo force perché a livello di pagina, il click è coperto da un altro elemento
                                                            // fattore che però a livello utente, non influenza il 'click'
    }
}

const confirm = () => {
    cy.get('[class="v-card v-sheet theme--light grey lighten-4"]').children('[class="v-card__actions"]').contains('Conferma').click()
}

enum priorities {'Bassa', 'Media', 'Alta'}

//priority 0 = low, 1 = medium, 2 = high
const createNewPlannedMaintenance = (description: string, priority: priorities, assetoToSearch: string, responsabileToSearch: string, frequencyType: string, frequency: string,  startDate: string, conclusionDate?: string,  prevDuration?: string, startHour?: string, floating?: boolean) => {
   
    startAddingNewPlannedMaintenance()
    insertGeneralita(description, priority)
    insertTimeInfos(startDate, conclusionDate, prevDuration, startHour)
    insertGestioneAsset(assetoToSearch)
    insertResponsabile(responsabileToSearch)
    chooseFrequenza(frequencyType, frequency, floating)
    cy.get('[class="v-btn__content"]').contains('Conferma').click()
    confirm()
}

export {
    openOrCloseLateralMenu,
    createNewPlannedMaintenance
}