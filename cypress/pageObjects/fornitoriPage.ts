const fornitoriPageUrl: string = 'http://localhost:8091/supplier'

const insertNome = (nome: string) => {
    cy.get('label').contains('Nome').siblings('input').type(nome)
}

const insertEmail = (email: string) => {
    cy.get('label').contains('Email').siblings('input').type(email)
}

const insertTelefono = (tel: string) => {
    cy.get('label').contains('Telefono').siblings('input').type(tel)
}

const insertCodFisc = (codFisc: string) => {
    cy.get('label').contains('Codice fiscale').siblings('input').type(codFisc)
}

const insertPIva = (PIva: string) => {
    cy.get('label').contains('Partita Iva').siblings('input').type(PIva)
}

const insertIndFatt = (ind: string) => {
    cy.get('label').contains('Indirizzo fatturazione').siblings('input').type(ind)
}

const insertDescr = (descr: string) => {
    cy.get('textarea').type(descr)
}

const confirm = () => {
    cy.get('button').contains('Conferma').click()
    cy.get('[class="v-card__actions"]').eq(1).contains('Conferma').click()
}

const insertRicercaGenerale = (ricerca: string) => {
    cy.get('label').contains('Ricerca generale').siblings('input').type(ricerca)
}

const insertFornitore = (fornitore: string) => {
    cy.get('label').contains('Cerca per fornitore').siblings('[type="text"]').click()
    cy.get('[class="v-list v-select-list v-sheet theme--light theme--light"]').children().contains(fornitore).click()
    cy.get('[d="M7,10L12,15L17,10H7Z"]').eq(0).click()
}

const addNewFornitore = (nome: string, telefono: string, codFisc: string, pIva: string, indFatturazione: string, descrizione?: string, email?: string) => {
    cy.url().should('equal', fornitoriPageUrl)
    cy.get('[d="M19,13H13V19H11V13H5V11H11V5H13V11H19V13Z"]').click()
    insertNome(nome)
    if(email) 
        insertEmail(email)
    insertTelefono(telefono)
    insertCodFisc(codFisc)
    insertPIva(pIva)
    insertIndFatt(indFatturazione)
    if(descrizione)
        insertDescr(descrizione)
    confirm()
}

const searchFornitore = (options?: {ricercaGenerale?: string, fornitore?: string}) => {
    cy.url().should('equal', fornitoriPageUrl)
    cy.get('[class="text-body-2 text-sm-h6 font-weight-medium"]').click()   //apro il menù di ricerca
    if(options?.ricercaGenerale)
        insertRicercaGenerale(options.ricercaGenerale)
    if(options?.fornitore)
        insertFornitore(options.fornitore)
}

const searchFornitoreWithExpectedNum = (expectedNum: number, options?: {ricercaGenerale?: string, fornitore?: string}) => {
    searchFornitore(options)
    if(expectedNum > 10)
        cy.get('[class="v-data-footer__pagination"]').should('contain.text', '1-10 of ' + expectedNum.toString())
    else 
        cy.get('[class="v-data-footer__pagination"]').should('contain.text', '1-'+ expectedNum.toString() + ' of ' + expectedNum.toString())
}

export {
    addNewFornitore,
    searchFornitore,
    searchFornitoreWithExpectedNum
}