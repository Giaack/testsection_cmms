function makeRndmString(length: number): string {
    let result = '';
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const charactersLength = characters.length;
    let counter = 0;
    while (counter < length) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
      counter += 1;
    }
    return result;
}

function getRandInt(max: number): number {
  return Math.floor(Math.random()*max + 1)
}

let monthToDaysMap = new Map([[1, 31], [2, 28], [3, 31], [4, 30], [5, 31], [6, 30], [7, 31], [8, 31], [9, 30], [10, 31], [11, 30], [12, 31]])

function getNumberOfClickToReachDate(startDate: string) {
  let dayOfStart = parseInt(startDate.split('/')[0])
  let monthOfStart = parseInt(startDate.split('/')[1])
  let yearOfStart = parseInt(startDate.split('/')[2])
  let todayDate = new Date()
  let todayDay = todayDate.getDate()
  let todayMonth = todayDate.getMonth() + 1
  let todayYear = todayDate.getFullYear()
  let yearDifferenceStart = yearOfStart - todayYear
  let monthDifferenceStart = monthOfStart - todayMonth
  let dayDifferenceStart = dayOfStart - todayDay
  let numberOfClicks = 0
  if(yearDifferenceStart < 0)
      throw new Error('difference in years can\'t be negative')
  if(yearDifferenceStart > 1) {
      numberOfClicks += (yearDifferenceStart-1) * 12
  }
  if(monthDifferenceStart > 0) {
      numberOfClicks += monthDifferenceStart
  }
  if(monthDifferenceStart == 0 && yearDifferenceStart == 1) {
      numberOfClicks += 12
  }
  if(monthDifferenceStart < 0) {
      numberOfClicks += (12+monthDifferenceStart)
  }
  let daysInMonth = monthToDaysMap.get(todayMonth)
  if(daysInMonth && todayDay + dayDifferenceStart > daysInMonth) {
      numberOfClicks += 1
  }
  return numberOfClicks
}

const loginPageUrl: string = 'http://localhost:8091/login'
const dashboardPageUrl: string = 'http://localhost:8091/dashboard'
const usersPageUrl: string = 'http://localhost:8091/users'
const odlPageUrl: string = 'http://localhost:8091/odl'
const plannedMaintenancePageUrl: string = 'http://localhost:8091/plannedMaintenance'
const maintainersPageUrl: string = 'http://localhost:8091/maintainers'
const structurePageUrl: string = 'http://localhost:8091/structure'
const assetPageUrl: string = 'http://localhost:8091/asset'
const customerPageUrl: string = 'http://localhost:8091/customer'
const supplierPageUrl: string = 'http://localhost:8091/supplier'
const rolesPageUrl: string = 'http://localhost:8091/roles'
const skillsPageUrl: string = 'http://localhost:8091/skills'
const assetHistoryPageUrl: string = 'http://localhost:8091/assetHistory'
const odlHistoryPageUrl: string = 'http://localhost:8091/ODLHistory'
const mpHistoryPageUrl: string = 'http://localhost:8091/MPHistory'

const openOrCloseLateralMenu = () => {
  cy.get('[class="v-btn v-btn--fab v-btn--icon v-btn--round theme--dark v-size--default"]').click({force: true})
}


const goToUsersPage = () => {
  openOrCloseLateralMenu()
  cy.get('[href="/users"]').click()
  cy.url().should('equal', usersPageUrl)
}

const goToOdlPage = () => {
  openOrCloseLateralMenu()
  cy.get('[href="/odl"]').click()
  cy.url().should('equal', odlPageUrl)
}

const goToPlannedMaintenancePage = () => {
  openOrCloseLateralMenu()
  cy.get('[href="/plannedMaintenance"]').click()
  cy.url().should('equal', plannedMaintenancePageUrl)
}

const goToManutentoriPage = () => {
  openRisorseSector()
  cy.get('[href="/maintainers"]').click()
  cy.url().should('equal', maintainersPageUrl)
}

const goToStructurePage = () => {
  openRisorseSector()
  cy.get('[href="/structure"]').click()
  cy.url().should('equal', structurePageUrl)
}

const goToAssetPage = () => {
  openRisorseSector()
  cy.get('[href="/asset"]').click()
  cy.url().should('equal', assetPageUrl)
}

const goToCustomerPage = () => {
  openRisorseSector()
  cy.get('[href="/customer"]').click()
  cy.url().should('equal', customerPageUrl)
}

const goToSupplierPage = () => {
  openRisorseSector()
  cy.get('[href="/supplier"]').click()
  cy.url().should('equal', supplierPageUrl)
}

const goToRolesPage = () => {
  openOrCloseLateralMenu()
  cy.get('[class="v-list-item__title"]').contains('Competenze').click()
  cy.get('[href="/roles"]').click()
  cy.url().should('equal', rolesPageUrl)
}

const goToSkillsPage = () => {
  openOrCloseLateralMenu()
  cy.get('[class="v-list-item__title"]').contains('Competenze').click()
  cy.get('[href="/skills"]').click()
  cy.url().should('equal', skillsPageUrl)
}

const goToAssetHistoryPage = () => {
  openOrCloseLateralMenu()
  cy.get('[class="v-list-item__title"]').contains('Storico').click()
  cy.get('[href="/assetHistory"]').click()
  cy.url().should('equal', assetHistoryPageUrl)
}

const goToODLHistoryPage = () => {
  openOrCloseLateralMenu()
  cy.get('[class="v-list-item__title"]').contains('Storico').click()
  cy.get('[href="/ODLHistory"]').click()
  cy.url().should('equal', odlHistoryPageUrl)
}

const goToMPHistoryPage = () => {
  openOrCloseLateralMenu()
  cy.get('[class="v-list-item__title"]').contains('Storico').click()
  cy.get('[href="/MPHistory"]').click()
  cy.url().should('equal', mpHistoryPageUrl)
}

const openRisorseSector = () => {
  openOrCloseLateralMenu()
  cy.get('[class="v-list-item__title"]').contains('Risorse').click()
}

export {
    makeRndmString,
    getRandInt,
    openOrCloseLateralMenu,
    goToUsersPage,
    goToOdlPage,
    goToPlannedMaintenancePage, 
    goToManutentoriPage,
    goToStructurePage,
    goToAssetPage,
    goToCustomerPage,
    goToSupplierPage,
    goToRolesPage,
    goToSkillsPage,
    goToAssetHistoryPage,
    goToODLHistoryPage,
    goToMPHistoryPage,
    getNumberOfClickToReachDate
}