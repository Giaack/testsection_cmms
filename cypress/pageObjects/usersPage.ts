import { makeRndmString } from "./utils"

const usersPageUrl: string = 'http://localhost:8091/users'

const openOrCloseLateralMenu = () => {
    cy.url().should('equal', usersPageUrl)
    cy.get('[class="v-btn v-btn--fab v-btn--icon v-btn--round theme--dark v-size--default"]').click({force: true})
}

/** la funzione ritorna la password automaticamente generata x l'utenza (esterno) */
const addUserEsterno = (email: string, gerarchia: string) => {
    let nomeUtente: string = email.split('@')[0]
    // let toRet: string
    cy.url().should('equal', usersPageUrl)
    cy.get('[class="elevation-2 v-btn v-btn--is-elevated v-btn--fab v-btn--has-bg v-btn--round theme--light v-size--small"]').click().then(() => {
        cy.get('label').contains('Crea utenza per esterno').click({force: true})
        cy.get('label').contains('Mail').siblings().click({force: true}).type(email)
        cy.get('label').contains('Nome utente').siblings().click({force: true}).type(nomeUtente)
        // cy.intercept('GET', 'http://localhost:8091/api/user/generateRandomPassword').as('getRandomPassword')
        cy.get('[class="v-btn v-btn--is-elevated v-btn--has-bg v-btn--rounded theme--light v-size--small"]').click({force: true})
        // cy.wait('@getRandomPassword').then((intercepted) => {
        //     if(intercepted.response) {
        //         toRet = intercepted.response.body
        //     }
        // })
        cy.get('label').contains('Gerarchie').siblings('input').click({force: true})
        cy.get('[class="v-list v-select-list v-sheet theme--light v-list--dense theme--light"]').children().contains(gerarchia).click()
        cy.get('[class="v-btn__content"]').contains('Conferma').click()
        cy.get('[class="v-btn v-btn--text theme--light v-size--default"]').eq(2).click()
        cy.wait(500)                
        // return toRet;
    })
}

const addUserManutentore = (gerarchia: string, manutentore: string) => {
    let toRet: string
    cy.url().should('equal', usersPageUrl)
    cy.get('[class="elevation-2 v-btn v-btn--is-elevated v-btn--fab v-btn--has-bg v-btn--round theme--light v-size--small"]').click()
    
    // .then(() => {
    //     cy.intercept('GET', 'http://localhost:8091/api/user/generateRandomPassword').as('getRandomPassword')
        cy.get('[class="v-btn v-btn--is-elevated v-btn--has-bg v-btn--rounded theme--light v-size--small"]').click({force: true})
        // cy.wait('@getRandomPassword').then((intercepted) => {
        //     if(intercepted.response) {
        //         toRet = intercepted.response.body
        //     }
        // })
        cy.get('label').contains('Nome utente').siblings('input').click({force: true}).type('randoUsername'+makeRndmString(5))
        cy.get('label').contains('Gerarchie').siblings('input').click({force: true})
        cy.get('[class="v-list v-select-list v-sheet theme--light v-list--dense theme--light"]').children().contains(gerarchia).click({force: true})
        cy.get('label').contains('Manutentori').siblings('input').click({force: true})
        // cy.get('[class="v-menu__content theme--light v-menu__content--fixed menuable__content__active"]').scrollTo('bottom')
        cy.get('[class="v-list v-select-list v-sheet theme--light v-list--dense theme--light"]').children().contains(manutentore).click({force: true})
        cy.get('[class="v-btn__content"]').contains('Conferma').click()
        cy.get('[class="v-btn v-btn--text theme--light v-size--default"]').eq(2).click()
        cy.wait(500)                
        // return toRet;
    // })
}

const checkNumberOfUserInResearch = (expectedNum: number) => {
    cy.url().should('equal', usersPageUrl)
    if(expectedNum > 10)
        cy.get('[class="v-data-footer__pagination"]').invoke('text').should('eq', '1-10 of '+ expectedNum +'')    
    else if (expectedNum == 0)
        cy.get('[class="v-data-footer__pagination"]').invoke('text').should('eq', '–')
    else
        cy.get('[class="v-data-footer__pagination"]').invoke('text').should('eq', '1-' + expectedNum + ' of '+ expectedNum +'')
}

const searchUser = (searchBarInput: string, active: boolean) => {   
    cy.url().should('equal', usersPageUrl)
    cy.get('[class="text-body-2 text-sm-h6 font-weight-medium"]').click()
    if(!active) {
        cy.get('[class="v-input--selection-controls__ripple orange--text text--darken-3"]').click()
    }
    cy.get('[type="text"]').eq(0).clear().type(searchBarInput)
    cy.get('[class="text-body-2 text-sm-h6 font-weight-medium"]').click()
}

const searchUserWithExpectedNum = (expectedNum: number, searchBarInput: string, active: boolean) => {
    searchUser(searchBarInput, active)
    checkNumberOfUserInResearch(expectedNum)
}

const checkNotAssociatedMaintainerAreActuallyNot = () => {
   cy.url().should('equal', usersPageUrl)
   let allUsersObject
   cy.intercept('GET', 'http://localhost:8091/api/user/getAllUsers').as('getAllUsers')
   cy.reload()
   // prendo l'oggetto contenente tutti gli users (quindi chi con un manutentore associato e chi no)
   cy.wait('@getAllUsers').then((intercepted) => {
    if(intercepted.response) {
        allUsersObject = intercepted.response.body
    }
   })
   cy.intercept('GET', 'http://localhost:8091/api/maintainer/getMaintainersWithoutUser').as('getMaintainersWithoutUser')
   cy.get('[class="elevation-2 v-btn v-btn--is-elevated v-btn--fab v-btn--has-bg v-btn--round theme--light v-size--small"]').click()   
   cy.wait(2000)
   let maintainersNoUserObject: any
   // prendo l'oggetto contenente tutti i profili di manutentori che non hanno un' utenza associata
   cy.wait('@getMaintainersWithoutUser').then((intercepted) => {
        if(intercepted.response) {
            maintainersNoUserObject = intercepted.response.body
        }
        let i: number = 0
        let allUsersMaintainerAssociated: any[] = []
        allUsersObject!.forEach((user: { maintainer: any }) => {
            allUsersMaintainerAssociated[i] = user.maintainer
            i++
        })

        //controllo che nessuno dei profili manutentori associati ad un utente sia presente nell'oggetto dei manutentori senza utente
        allUsersMaintainerAssociated.forEach(user => {
            if(user in maintainersNoUserObject)
                throw new Error('Errore, un manutentore che non dovrebbe essere associato ha un utente associato')
        });
   })
}

export {
    openOrCloseLateralMenu,
    addUserEsterno,
    addUserManutentore,
    searchUser,
    checkNumberOfUserInResearch,
    searchUserWithExpectedNum,
    checkNotAssociatedMaintainerAreActuallyNot
}