const structurePageUrl: string = 'http://localhost:8091/structure'

const insertGeneralSearch = (generalSearch: string) => {
    cy.get('label').contains('Ricerca generale').siblings('input').type(generalSearch)
}

const insertStruttura = (struttura: string) => {
    cy.get('[d="M7,10L12,15L17,10H7Z"]').eq(0).click()
    cy.get('[class="v-menu__content theme--light menuable__content__active v-autocomplete__content"]').children().contains(struttura, {matchCase:true}).click()
}

const insertCliente = (cliente: string) => {
    cy.get('[d="M7,10L12,15L17,10H7Z"]').eq(1).click()
    cy.get('[class="v-menu__content theme--light menuable__content__active v-autocomplete__content"]').children().contains(cliente).click()
}   //non va bene perché non sceglie quello che c'è nella scritta,  ma quello che lo contiene 'test' -> 'test', 'testandreea' test 1'...

const searchStructure = (options?:{generalSearch?: string, perStruttura?: string, perCliente?: string}) => {
    cy.url().should('equal', structurePageUrl)
    cy.get('[class="text-body-2 text-sm-h6 font-weight-medium"]').click()
    if(options?.generalSearch) 
        insertGeneralSearch(options.generalSearch)
    if(options?.perStruttura)
        insertStruttura(options.perStruttura)
    if(options?.perCliente)
        insertCliente(options.perCliente)
}

const searchStructureAndExpectedTotal = (expectedNum: number, options?:{generalSearch?: string, perStruttura?: string, perCliente?: string}) => {
    searchStructure(options)
    if(expectedNum > 10)
        cy.get('[class="v-data-footer__pagination"]').invoke('text').should('eq', '1-10 of '+ expectedNum +'')    
    else
        cy.get('[class="v-data-footer__pagination"]').invoke('text').should('eq', '1-' + expectedNum + ' of '+ expectedNum +'')
}

// NEED TO ADD 'ASSOCIA PIANI' FEATURE -> not working in main yet

export {
    searchStructure,
    searchStructureAndExpectedTotal
}