// Object relative to Login Page (http://localhost:8091/login)

//defining the object loginPage :


const loginPage = {

    loginPageUrl: 'http://localhost:8091/login',
    dashboardPageUrl: 'http://localhost:8091/dashboard',

    insertEmail: (email: string) => {
        cy.get('[type="text"]').focus().type(email)
    },
    insertPassword: (pwd: string) => {
        cy.get('[type="password"]').focus().type(pwd)
    },
    submitLoginForm: () => {
        cy.get('[class="v-btn v-btn--block v-btn--is-elevated v-btn--has-bg theme--light v-size--default"]').click()
    },
    performLogin: (email: string, password: string) => {
        loginPage.insertEmail(email)
        loginPage.insertPassword(password)
        loginPage.submitLoginForm()
    }
}

const loginPageUrl: string = 'http://localhost:8091/login'
const dashboardPageUrl: string = 'http://localhost:8091/dashboard'

// const insertEmail = (email: string) => {
//     // cy.url().should('equal', loginPageUrl)
//     cy.get('[type="text"]').focus().type(email)
// }

// const insertPassword = (pwd: string) => {
//     // cy.url().should('equal', loginPageUrl)
//     cy.get('[type="password"]').focus().type(pwd)
// }

// const submitLoginForm = () => {
//     // cy.url().should('equal', loginPageUrl)
//     // cy.get('[type="submit"]').click({multiple: true})
//     cy.get('[class="v-btn v-btn--block v-btn--is-elevated v-btn--has-bg theme--light v-size--default"]').click()
// }

// const performLogin = (email: string, password: string) => {
//     insertEmail(email)
//     insertPassword(password)
//     submitLoginForm()
//     cy.wait(45000)
//     // cy.url().should('equal','http://localhost:8091/dashboard')
// }

// const performLoginStub = () => {
//     let user = {
//         "data": {
//             "token": "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ0ZXN0MUBkb21pbmlvLml0IiwiaGllcmFyY2h5SUQiOiIxIiwiY29kQ29tcGFueSI6IjEiLCJpYXQiOjE3MDEzMzEzNDMsImV4cCI6MTcwMTQxNzc0M30.WXOgJkRg1pgAs0XYtnnevKmCBGhZp3DMV0BT9MyEw6c"
//         },
//         "status": 200,
//         "statusText": "OK",
//         "headers": {
//             "cache-control": "no-cache, no-store, max-age=0, must-revalidate",
//             "connection": "close",
//             "content-encoding": "gzip",
//             "content-type": "application/json",
//             "date": "Thu, 30 Nov 2023 08:02:23 GMT",
//             "expires": "0",
//             "pragma": "no-cache",
//             "transfer-encoding": "chunked",
//             "vary": "Accept-Encoding",
//             "x-content-type-options": "nosniff",
//             "x-frame-options": "DENY",
//             "x-powered-by": "Express",
//             "x-xss-protection": "0"
//         },
//         "config": {
//             "transitional": {
//                 "silentJSONParsing": true,
//                 "forcedJSONParsing": true,
//                 "clarifyTimeoutError": false
//             },
//             "adapter": [
//                 "xhr",
//                 "http"
//             ],
//             "transformRequest": [
//                 null
//             ],
//             "transformResponse": [
//                 null
//             ],
//             "timeout": 0,
//             "xsrfCookieName": "XSRF-TOKEN",
//             "xsrfHeaderName": "X-XSRF-TOKEN",
//             "maxContentLength": -1,
//             "maxBodyLength": -1,
//             "env": {},
//             "headers": {
//                 "Accept": "application/json, text/plain, */*",
//                 "Content-Type": "application/json"
//             },
//             "method": "post",
//             "url": "api/v1/auth/authenticate",
//             "data": "{\"userName\":\"test1@dominio.it\",\"userPass\":\"4321\"}"
//         },
//         "request": {}
//     }
//     let stringified = JSON.stringify(user)
//     if(window.localStorage.getItem('user') != stringified )
//         window.localStorage.setItem('user', JSON.stringify(user))
// }

export {
    loginPage
}
