// interface loginPage {
//     visit(): void;
//     insertEmail(email: string): void;
//     insertPassword(password: string): void;
//     submitLoginForm(): void;
//     performLogin(email: string, password: string): void;
// }

var loginPage = {

    visit: function () {
        cy.visit('http://localhost:8091/login')
    },

    insertEmail: function (email: string) {
        cy.get('[type="text"]').focus().type(email)
    },

    insertPassword: function (pwd: string) {
        cy.get('[type="password"]').focus().type(pwd)
    },

    submitLoginForm: function () {
        cy.get('[class="v-btn v-btn--block v-btn--is-elevated v-btn--has-bg theme--light v-size--default"]').click()
    },

    performLogin: function (email: string, password: string) {
        loginPage.insertEmail(email)
        loginPage.insertPassword(password)
        loginPage.submitLoginForm()
    }
}

export { loginPage }