
## Eliminare un ODL (e/o Richiesta_Intervento)
1. elimina lo *Storico_ODL associato*
2. elimina l' *ODL associato*
3. elimina la *richiesta intervento* 
4. elimina lo *strocio_richiesta intervento* (in caso di storico presente) 


## Eliminare una MP



## Eliminare un manutentore
1. Elimino in Users (no conflitti)
2. Elimino da Ruolo_Manutentore
3. Elimino da Skill_Manutentore
4. Elimino da Storico_ODL (se ce n'è associati)
5. Elimina da Richiesta_Intervento (se ce n'è associati)
    1. Elimino l'ODL (se ce v'è associato)
6. Elimino da Manutentore
7. Elimino la Anagrafica_Manutentore associata



## Eliminare un ruolo
1. Eliminare in Storico_Ruolo_Manutentore
2. Eliminare in Ruolo_Manutentore
3. Eliminare il Ruolo



## Eliminare una skill
0. (eliminare in Storico_Skill_Manutentore)
1. Eliminare in Skill_Manutentore
2. Eliminare la Skill