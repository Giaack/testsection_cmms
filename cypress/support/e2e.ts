// ***********************************************************
// This example support/e2e.ts is processed and
// loaded automatically before your test files.
//
// This is a great place to put global configuration and
// behavior that modifies Cypress.
//
// You can change the location of this file or turn off
// automatically serving support files with the
// 'supportFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/configuration
// ***********************************************************

// Import commands.js using ES2015 syntax:
import './commands'
import {loginPage} from "../pageObjects/loginPage"
import {adminEmail, adminPassword, adminName, adminSurname, adminRole} from "./utils"

/*Login doesn't work automatically due to a cypress related (??) problem, so we need to insert datas manually to log-in, but at least it works like that
it is a problem due to the request made to 'http://localhost:8091/api/v1/auth/authenticate', it works manually but it doesn't if it is cypress to do it */
// beforeEach('should login before actual Test', () => {
//     cy.visit('http://localhost:8091/login')
//         loginPage.performLogin(adminEmail,adminPassword)
// })

beforeEach('should stub the login before each Test', () => {
    cy.visit('http://localhost:8091/login')
    loginPage.performLogin(adminEmail,adminPassword)
})

// Alternatively you can use CommonJS syntax:
// require('./commands')