/** This Tests page is used to check that all the reuqests that involve database, use null where a field can be null, and not a default value (as 0, empty string ecc..) */
import * as dashboardPage from "../../pageObjects/dashboardPage"
import * as usersPage from "../../pageObjects/usersPage"
import * as odlPage from "../../pageObjects/odlPage"
import * as plannedMaintenancePage from "../../pageObjects/plannedMaintenancePage"
import * as assetPage from "../../pageObjects/assetPage"
import * as customerPage from "../../pageObjects/clientiPage"
import * as structurePage from "../../pageObjects/structurePage"
import * as fornitoriPage from "../../pageObjects/fornitoriPage"
import * as manutentoriPage from "../../pageObjects/maintainersPage"
import * as rolePage from "../../pageObjects/ruoliPage"
import * as skillPage from "../../pageObjects/abilitaPage"

import { makeRndmString, getRandInt, goToOdlPage } from "../../pageObjects/utils"
import { adminEmail, adminPassword, adminName, adminSurname, adminRole } from "../../support/utils"

describe('Test null field to be null', () => {
    it('should test that a new ODL insert, use the right field nullability rules', () => {
        dashboardPage.goToOdlPage()
        // lasciamo vuoto il campo note e i campi relativi a date/tempistiche, che sono i campi nullabili, per vedere che effettivamente saranno così
        cy.intercept('POST', 'http://localhost:8091/api/interventionRequest/saveNewInterventionRequest').as('odlResponse')
        let descrizione = 'descr '+ makeRndmString(10)
        let priorita = 'Bassa'
        let categoria = 'Correttiva'
        let responsabile = 'JET67BCHRT19SQ9L'
        let asset = 'AA-000001'
        odlPage.insertNewOdl(descrizione, priorita, categoria, responsabile, asset)
        cy.wait('@odlResponse').then((intercepted) => {
            if(intercepted.response){
                let todate = new Date()
                let day = todate.getDate().toString()
                let month = (todate.getMonth() + 1).toString()
                let year = todate.getFullYear().toString()
                assert(intercepted.response.body.expectedDuration === null)
                assert(intercepted.response.body.expectedEndDate === null)
                assert (intercepted.response.body.expectedStartDate === null)
                assert (intercepted.response.body.expectedTime === null)
                assert (intercepted.response.body.note === null)
                assert (intercepted.response.body.lastUpdate === year+'-'+month+'-'+day)
                assert (intercepted.response.body.requestedDate === year+'-'+month+'-'+day)
                assert (intercepted.response.body.id !== null && intercepted.response.body.id !== '')
                assert (intercepted.response.body.description !== null && intercepted.response.body.description !== '')
            }
        })
    }); //test is working 

    it('should test that a new MP insert, use the right field nullability rules ', () => {
        dashboardPage.goToPlannedMaintenancePage()
        cy.intercept('POST','http://localhost:8091/api/scheduled/saveScheduledMaintainance').as('mpResponse')
        let descrizione = 'descr '+ makeRndmString(10)
        let priorita = 'Bassa'
        let asset = 'AA-000001'
        let responsabile = 'JET67BCHRT19SQ9L'
        plannedMaintenancePage.createNewPlannedMaintenance(descrizione, 0, asset, responsabile, 'settimanale', '2', '18/03/2024')
        cy.wait('@mpResponse').then((intercepted) => {  
            if(intercepted.response) {
                // id, description, startDate, endDate?, newAssets, expectedDuration?, startingTime?, , floating, cyclesNumber, machineDowntime, codCompany
                assert(intercepted.response.body.id !== null && intercepted.response.body.id !== '')
                assert(intercepted.response.body.description !== null && intercepted.response.body.description !== '')
                assert(intercepted.response.body.startDate !== null && intercepted.response.body.startDate !== '')
                assert(intercepted.response.body.endDate === null)
                assert(intercepted.response.body.newAssets === false) 
                assert(intercepted.response.body.expectedDuration === null)
                assert(intercepted.response.body.startingTime === null)
                assert(intercepted.response.body.floating === false)
            }
        })
    }); //test is working

    it('should test modify in maintainers page, use the right field nullability rules', () => {
        //http://localhost:8091/api/maintainer/modifyMaintainer
        dashboardPage.goToManutentoriPage()
        cy.intercept('PUT','http://localhost:8091/api/maintainer/modifyMaintainer').as('modifyMaintainer')
        //usiamo il codice fiscale del manutentore come 'chiave' per andare ad operare sempre su di esso, ad ogni run del test : gcd2109nv
        let nome: string = 'new' + makeRndmString(10)
        let cognome: string = 'new' + makeRndmString(10)
        let residenza: string = 'new' + makeRndmString(10)
        let email: string = makeRndmString(10) + '@mail.new'
        let telefono: string = makeRndmString(10)
        manutentoriPage.modifyMantainer({nome: nome, cognome: cognome, residenza: residenza, email: email, telefono: telefono }, {generalSearch: 'gcd2109nv'})
        cy.wait('@modifyMaintainer').then((intercepted) => {
            if(intercepted.response) {
                assert(intercepted.response.body.codManutentore !== null && intercepted.response.body.codManutentore !== '')
                assert(intercepted.response.body.email !== null && intercepted.response.body.email !== '')
                assert(intercepted.response.body.telefono !== null && intercepted.response.body.telefono !== '')
                assert(intercepted.response.body.attivo !== null)
                assert(intercepted.response.body.maintainerRegistryEntity.nome !== null && intercepted.response.body.maintainerRegistryEntity.nome !== '')
                assert(intercepted.response.body.maintainerRegistryEntity.cognome !== null && intercepted.response.body.maintainerRegistryEntity.cognome !== '')
                assert(intercepted.response.body.maintainerRegistryEntity.codFiscale !== null && intercepted.response.body.maintainerRegistryEntity.codFiscale !== '')
                assert(intercepted.response.body.maintainerRegistryEntity.cittaResidenza !== null && intercepted.response.body.maintainerRegistryEntity.cittaResidenza !== '')
                assert(intercepted.response.body.maintainerRegistryEntity.id !== null && intercepted.response.body.maintainerRegistryEntity.id !== '')
            }
        })
    });

    it('should test nullability rules are satisfied during maintainer creation', () => {
        dashboardPage.goToManutentoriPage()
        cy.intercept('POST', 'http://localhost:8091/api/maintainer/newMaintainer').as('newMaintainer')
        let name: string = makeRndmString(15)
        let surname: string = makeRndmString(15)
        let codFisc: string = makeRndmString(10)
        let email: string = makeRndmString(10) + '@cmmail.new'
        manutentoriPage.addManutentore(name, surname, codFisc, email)
        cy.wait('@newMaintainer').then((intercepted) => { 
            if(intercepted.response) {
                assert(intercepted.response.body.codManutentore !== null && intercepted.response.body.codManutentore !== '')
                assert(intercepted.response.body.email !== null && intercepted.response.body.email !== '')
                assert(intercepted.response.body.telefono === null)
                assert(intercepted.response.body.attivo !== null)
                assert(intercepted.response.body.codCompany !== null)
                assert(intercepted.response.body.maintainerRegistryEntity.nome !== null && intercepted.response.body.maintainerRegistryEntity.nome !== '')
                assert(intercepted.response.body.maintainerRegistryEntity.cognome !== null && intercepted.response.body.maintainerRegistryEntity.cognome !== '')
                assert(intercepted.response.body.maintainerRegistryEntity.codFiscale !== null && intercepted.response.body.maintainerRegistryEntity.codFiscale !== '')
                assert(intercepted.response.body.maintainerRegistryEntity.cittaResidenza === null)
                assert(intercepted.response.body.maintainerRegistryEntity.id !== null && intercepted.response.body.maintainerRegistryEntity.id !== '')
            }
        })
    });

    // it('should test nullability rules in user insert', () => {
    //     dashboardPage.goToUsersPage()
    //     let username: string = makeRndmString(10)
    //     let password: string = makeRndmString(10)
    //     let gerarchia: string = 'Tecnico'
    //     usersPage.addUser(username, password, gerarchia)
    // });

    it('when this test will fail (maintainers page), it will be ok', () => {
        //http://localhost:8091/api/maintainer/modifyMaintainer
        dashboardPage.goToManutentoriPage()
        cy.intercept('PUT','http://localhost:8091/api/maintainer/modifyMaintainer').as('modifyMaintainer')
        //usiamo il codice fiscale del manutentore come 'chiave' per andare ad operare sempre su di esso, ad ogni run del test : gcd2109nv
        let nome: string = ''
        let cognome: string = ''
        let residenza: string = ''
        let email: string = ''
        let telefono: string = ''
        manutentoriPage.modifyMantainerNull(nome, cognome, residenza, email, telefono, {generalSearch: 'gcd2109nv'})
        cy.wait('@modifyMaintainer').then((intercepted) => {
            if(intercepted.response) {
                assert(intercepted.response.body.codManutentore !== null)
                assert(intercepted.response.body.email === null || intercepted.response.body.email === '')
                assert(intercepted.response.body.telefono === null || intercepted.response.body.telefono === '')
                assert(intercepted.response.body.attivo !== null)
                assert(intercepted.response.body.maintainerRegistryEntity.nome === null || intercepted.response.body.maintainerRegistryEntity.nome === '')
                assert(intercepted.response.body.maintainerRegistryEntity.cognome === null || intercepted.response.body.maintainerRegistryEntity.cognome === '')
                assert(intercepted.response.body.maintainerRegistryEntity.codFiscale !== null && intercepted.response.body.maintainerRegistryEntity.codFiscale !== '')
                assert(intercepted.response.body.maintainerRegistryEntity.cittaResidenza === null || intercepted.response.body.maintainerRegistryEntity.cittaResidenza === '')
                assert(intercepted.response.body.maintainerRegistryEntity.id !== null && intercepted.response.body.maintainerRegistryEntity.id !== '')
            }
        })
    });

    it('should test that asset insert has not nullable fields, as nullability rules ask', () => {
        dashboardPage.goToAssetPage()
        cy.intercept('POST','http://localhost:8091/api/asset/saveAsset').as('assetResponse')
        let numSeriale = 'AA-' + makeRndmString(6)
        let descrizione = makeRndmString(10)
        let categoria = 'Collatura'
        let stato = 'Attivo'
        let edificio = 'Officina Xcorp'
        let piano = 'Piano Terra'
        let area = 'officina vera e propria'
        assetPage.addNewAsset(numSeriale, descrizione, categoria, stato, edificio, piano, area)
        cy.wait('@assetResponse').then((intercepted) => {
            if(intercepted.response) {
                assert(intercepted.response.body.id !== null && intercepted.response.body.id !== '')
                assert(intercepted.response.body.serialNumber !== null && intercepted.response.body.serialNumber !== '')
                assert(intercepted.response.body.description !== null && intercepted.response.body.description !== '')
                assert(intercepted.response.body.assetCategoryEntity !== null)
                assert(intercepted.response.body.assetStateEntity !== null)
                assert(intercepted.response.body.areaEntity !== null)
                assert(intercepted.response.body.active !== null)
            }
        })
    });

    it('should test insert in customers page, use the right field nullability rules ', () => {
        dashboardPage.goToCustomerPage()
        cy.intercept('POST','http://localhost:8091/api/customer/addNewCustomer').as('customerAddResponse')
        let nome: string = 'new' + makeRndmString(10)
        let email: string = makeRndmString(10) + '@mail.new'
        let telefono: string = makeRndmString(10)
        let codFisc: string = makeRndmString(10)
        let pIva : string = makeRndmString(10)
        let fattIndirizzo: string = makeRndmString(10)
        let descr: string = makeRndmString(10)
        customerPage.addCustomer(nome, email, telefono, codFisc, pIva, fattIndirizzo, descr)
        cy.wait('@customerAddResponse').then((intercepted) => {
            if(intercepted.response) {
                assert(intercepted.response.body.id !== null)
                console.log('intercepted.response.body.id : ', intercepted.response.body.id)
                assert(intercepted.response.body.email !== null)
                console.log('intercepted.response.body.email : ', intercepted.response.body.email)
                assert(intercepted.response.body.telephone !== null)
                console.log('intercepted.response.body.telephone : ', intercepted.response.body.telephone)
                assert(intercepted.response.body.codCompany !== null)
                console.log('intercepted.response.body.codCompany : ', intercepted.response.body.codCompany)
                assert(intercepted.response.body.customerRegistryEntity.id !== null)
                console.log('intercepted.response.body.customerRegistryEntity.id : ', intercepted.response.body.customerRegistryEntity.id)
                assert(intercepted.response.body.customerRegistryEntity.name !== null)
                console.log('intercepted.response.body.customerRegistryEntity.name : ', intercepted.response.body.customerRegistryEntity.name)
                assert(intercepted.response.body.customerRegistryEntity.description !== null)
                console.log('intercepted.response.body.customerRegistryEntity.description : ', intercepted.response.body.customerRegistryEntity.description)
                assert(intercepted.response.body.customerRegistryEntity.taxCode !== null)
                console.log('intercepted.response.body.customerRegistryEntity.taxCode : ', intercepted.response.body.customerRegistryEntity.taxCode)
                assert(intercepted.response.body.customerRegistryEntity.vatNumber !== null)
                console.log('intercepted.response.body.customerRegistryEntity.vatNumber : ', intercepted.response.body.customerRegistryEntity.vatNumber)
                assert(intercepted.response.body.customerRegistryEntity.billingAddress !== null)
                console.log('intercepted.response.body.customerRegistryEntity.billingAddress : ', intercepted.response.body.customerRegistryEntity.billingAddress)
            }
        })
    });

    it.skip('should test that adding a structure to a customer, use the right field nullability rules', () => {
        /**@todo because it's not implemented yet in main project */
    });
    
    it('should test that adding a new fornitore in supplier page, use the right field nullability rules - test will pass when email nullability bur will be', () => {
        dashboardPage.goToSupplierPage()
        // nome, telefono, codFisc, pIva, indFatturazione, descrizione?, email?
        cy.intercept('http://localhost:8091/api/supplier/addNewSupplier').as('newFornitore')
        let nome = 'new' + makeRndmString(10)
        let telefono = makeRndmString(10)
        let codFisc = makeRndmString(10)
        let pIva = makeRndmString(10)
        let indFatturazione = makeRndmString(10)
        let descrizione = makeRndmString(10) //la descrizione sarà da togliere, perché il DB la ammette not null, c'è un prob a liv user interface
        fornitoriPage.addNewFornitore(nome, telefono, codFisc, pIva, indFatturazione, descrizione)
        cy.wait('@newFornitore').then((intercepted) => {
            if(intercepted.response) {
                assert(intercepted.response.body.id !== null && intercepted.response.body.id !== '')
                assert(intercepted.response.body.email === null)
                assert(intercepted.response.body.telephone !== null && intercepted.response.body.telephone !== '')
                assert(intercepted.response.body.codCompany !== null && intercepted.response.body.codompany !== '')
                assert(intercepted.response.body.supplierRegistry.id !== null && intercepted.response.body.supplierRegistry.id !== '')
                assert(intercepted.response.body.supplierRegistry.name !== null && intercepted.response.body.supplierRegistry.name !== '')
                assert(intercepted.response.body.supplierRegistry.taxCode !== null && intercepted.response.body.supplierRegistry.taxCode !== '')
                assert(intercepted.response.body.supplierRegistry.billingAddress !== null && intercepted.response.body.supplierRegistry.billingAddress !== '')
                assert(intercepted.response.body.supplierRegistry.description !== null && intercepted.response.body.supplierRegistry.description !== '') // sarà da cambiare perché la descrizione dovrebbe ammettere null
            }
        })
    }); //test is not satisfied (as expected), it will be when email insert will be fixed

    it('should test adding a new Role, satisfies the expected nullability rules', () => {
        dashboardPage.goToRolesPage()
        cy.intercept('POST', 'http://localhost:8091/api/role/newRole').as('addRole')
        let descrizioneTipo = 'tipo' + makeRndmString(5)
        let noteDescrizione = 'note' + makeRndmString(10) //è nullabile, ma non è possibile lasciarlo vuoto, perché a livello utente è ichiesto l'inserimento, anche se a livello DB è nullable
        rolePage.addRuolo(descrizioneTipo, noteDescrizione)
        cy.wait('@addRole').then((intercept) => {
            if(intercept.response) {
                assert(intercept.response.body.codRuolo !== null)
                assert(intercept.response.body.descrizione !== null && intercept.response.body.descrizione !== '')
                assert(intercept.response.body.note !== null && intercept.response.body.note !== '')
                assert(intercept.response.body.codCompany !== null)
                assert(intercept.response.body.active !== null)
            }
        })
    });

    it('should test modifying a Role, satisfies the expected nullability rules - test will pass when note nullability will be fixed', () => {
        dashboardPage.goToRolesPage()
        cy.intercept('PUT', 'http://localhost:8091/api/role/modifyRole').as('modifyRole')
        let descrizioneTipo = 'tipo' + makeRndmString(5)
        rolePage.modifyRuoloNull(descrizioneTipo, {generalSearch: 'RoleDaMod'})
        cy.wait('@modifyRole').then((intercept) => {
            if(intercept.response) {
                assert(intercept.response.body.codRuolo !== null)
                assert(intercept.response.body.descrizione !== null && intercept.response.body.descrizione !== '')
                assert(intercept.response.body.note === null) 
                assert(intercept.response.body.codCompany !== null)
                assert(intercept.response.body.active !== null)
            }
        })
    });

    it('should test adding a new Skill, satisfies the expected nullability rules ', () => {
        dashboardPage.goToSkillsPage()
        cy.intercept('POST', 'http://localhost:8091/api/skill/newSkill').as('addSkill')
        let descrizioneTipo = 'tipo' + makeRndmString(5)
        let noteDescrizione = 'note' + makeRndmString(10) //è nullabile nel db, ma c'è un problema a livello utente (da fixare, per ora lo inseriamo ma non dovremmo)
        skillPage.addSkill(descrizioneTipo, noteDescrizione)
        cy.wait('@addSkill').then((intercept) => {
            if(intercept.response) {
                assert(intercept.response.body.codSkil !== null)
                assert(intercept.response.body.descrizione != null && intercept.response.body.descrizione !== '')
                assert(intercept.response.body.note !== null && intercept.response.body.note !== '')
                assert(intercept.response.body.codCompany !== null)
                assert(intercept.response.body.active !== null)
            }    
        })
    })

    it('should test modifying a skill, satisfies the expected nullability roles - test will pass when note nullability bug will be fixed', () => {
        dashboardPage.goToSkillsPage()
        cy.intercept('PUT', 'http://localhost:8091/api/skill/modifySkill').as('modifySkill')
        let decrizioneTipo = 'tipo' + makeRndmString(5)
        skillPage.modifySkillNull(decrizioneTipo, {generalSearch: 'AbDaModificare'})
        cy.wait('@modifySkill').then((intercept => {
            if(intercept.response) {
                assert(intercept.response.body.codSkil !== null)
                assert(intercept.response.body.descrizione != null && intercept.response.body.descrizione !== '')
                assert(intercept.response.body.note === null)
                assert(intercept.response.body.codCompany !== null)
                assert(intercept.response.body.active !== null)
            }    
        }))
    });
});


