import * as dashboardPage from "../../pageObjects/dashboardPage"
import * as usersPage from "../../pageObjects/usersPage"
import * as odlPage from "../../pageObjects/odlPage"
import * as plannedMaintenancePage from "../../pageObjects/plannedMaintenancePage"
import * as assetPage from "../../pageObjects/assetPage"
import * as customerPage from "../../pageObjects/clientiPage"
import * as structurePage from "../../pageObjects/structurePage"
import * as fornitoriPage from "../../pageObjects/fornitoriPage"
import * as manutentoriPage from "../../pageObjects/maintainersPage"
import * as rolePage from "../../pageObjects/ruoliPage"
import * as skillPage from "../../pageObjects/abilitaPage"

import { makeRndmString, getRandInt, goToOdlPage, goToManutentoriPage, goToUsersPage } from "../../pageObjects/utils"
import { adminEmail, adminPassword, adminName, adminSurname, adminRole } from "../../support/utils"

const dashboardPageUrl: string = 'http://localhost:8091/dashboard'

const addNewUserMaintainer = (gerarchia: string, name: string, surname: string, residenza: string, codiceFiscale: string, email: string, telefono: string, ruolo?: string, abilita?: string) => {
    let manutentore: string = name + '-' + surname
    cy.reload()
    goToManutentoriPage()
    manutentoriPage.addManutentore(name, surname, codiceFiscale, email, {residenza: residenza, telefono: telefono, ruolo: ruolo, abilita: abilita})
    cy.reload()
    goToUsersPage()
    usersPage.addUserManutentore(gerarchia, manutentore)
    usersPage.searchUserWithExpectedNum(1, email, true)
}

// LOGIN + DASHBOARD pages
describe('should test authentication and moving around the site (checking redirection)', () => {
    it('should check that login works, with the wanted profile (Mario Rossi) and role (Amministratore)', () => {
        //login is in the BeforeEach so.. we need to check that the we have been redirected to dashboard page 
        cy.url().should('equal',dashboardPageUrl)
        dashboardPage.checkLoggedUser(adminName, adminSurname)
        cy.visit(dashboardPageUrl)  //torno alla pagina 'senza modifiche' perché il menù laterale si chiuderebbe con la 'checkLoggedUserRole(:string)'
        dashboardPage.checkLoggedUserRole(adminRole)
    });

    it('should check foreground redirections work (to users page, to odl page, to plannedMaintenance)', () => {
        cy.visit(dashboardPageUrl)
        dashboardPage.goToUsersPage()
        cy.visit(dashboardPageUrl)
        dashboardPage.goToOdlPage()
        cy.visit(dashboardPageUrl)
        dashboardPage.goToPlannedMaintenancePage()
    })
    
    it('should test Risorse redirections work (to maintainers page, to structure page, to asset page, to customer page, to supplier page)', () => {
        dashboardPage.goToManutentoriPage()
        cy.visit(dashboardPageUrl)
        dashboardPage.goToStructurePage()
        cy.visit(dashboardPageUrl)
        dashboardPage.goToAssetPage()
        cy.visit(dashboardPageUrl)
        dashboardPage.goToCustomerPage()
        cy.visit(dashboardPageUrl)
        dashboardPage.goToSupplierPage()
    })

    it('should test Competenze redirections work (to roles page, to skills page)', () => {
        dashboardPage.goToRolesPage()
        cy.visit(dashboardPageUrl)
        dashboardPage.goToSkillsPage()
    })
        
    it('should test Storico redirections work (to assetHistory page, to ODLHistory, to MPHistory page)', () => {
        dashboardPage.goToAssetHistoryPage()
        cy.visit(dashboardPageUrl)
        dashboardPage.goToODLHistoryPage()
        cy.visit(dashboardPageUrl)
        dashboardPage.goToMPHistoryPage()
    })
}) //they work

// USER page
describe('should test users Page to work as intended', () => {

    it('should create a new user and associate him to a maintainer', () => {
        dashboardPage.goToUsersPage()
        addNewUserMaintainer('Tecnico', makeRndmString(Math.random()*10 + 1), makeRndmString(Math.random()*10 + 1), makeRndmString(Math.random()*10 + 1), makeRndmString(Math.random()*10 + 1), makeRndmString(Math.random()*10 + 1) + '@mail.cmms', makeRndmString(10), 'Meccanico', 'Patente C')
    });     //  => the test works, it's commented bc it would create ton of users

    it('should add a new user esterno, so a user without a maintainer profile linked to him', () => {
        dashboardPage.goToUsersPage()
        usersPage.addUserEsterno(makeRndmString(Math.random()*10 + 1) + '@mail.cmms', 'Tecnico')
    });

    it('should check that the search bar actually works', () => {
        dashboardPage.goToUsersPage()
        usersPage.searchUser('string', true)
        usersPage.checkNumberOfUserInResearch(1)
    })  //test is working

    it('should check number of administrator role users, is 5', () => {
        dashboardPage.goToUsersPage()
        usersPage.searchUser('Amministratore',true)
        usersPage.checkNumberOfUserInResearch(5)
    })  // => TEST IS WORKING 
})

//ODL Page          => cy.reload() is used to clear inputs
describe('should test odl page to work as intended', () => {
    it('should perform a research in odl page, then perform the research of the same odl, using different parameters and checking that the number of odl found is 1', () => {
        dashboardPage.goToOdlPage()
        // odlPage.searchOdl({descrizione: 'Controllare il ponte sollevatore', stato: 'Abortito', tipologia: 'Correttiva', priorita: 'Alta'})
        // cy.reload()
        odlPage.searchOdlAndExpectedTotal(1, {descrizione: 'Controllare il ponte sollevatore', stato: 'Abortito', tipologia: 'Correttiva'})
        cy.reload()
        odlPage.searchOdlAndExpectedTotal(1, {descrizione: 'Controllare il ponte sollevatore', stato: 'Abortito'})
        cy.reload()
        odlPage.searchOdlAndExpectedTotal(1, {descrizione: 'Controllare il ponte sollevatore', stato: 'Abortito', responsabile: 'Mario Rossi'})
    });

    it('should add a new odl with all values and then we search for that odl to actually exist', () => {
        dashboardPage.goToOdlPage()
        let descrizProva = 'to do : ' + makeRndmString(Math.floor(Math.random()*25) + 1)
        odlPage.insertNewOdl(descrizProva, 'Alta', 'Correttiva','JET67BCHRT19SQ9L', 'AA-000001', 'note random : ' + makeRndmString(Math.floor(Math.random()*10) + 1))
        odlPage.searchOdlAndExpectedTotal(1,{descrizione: descrizProva, priorita: 'Alta', tipologia: 'Correttiva'})
    });

    it('should add a new odl but without notes and check if it exists', () => {
        dashboardPage.goToOdlPage()
        let descrizProva = 'to do : ' + makeRndmString(Math.floor(Math.random()*25) + 1)
        odlPage.insertNewOdl(descrizProva, 'Alta', 'Correttiva','JET67BCHRT19SQ9L', 'AA-000001')
        odlPage.searchOdlAndExpectedTotal(1,{descrizione: descrizProva, priorita: 'Alta', tipologia: 'Correttiva'})
    });

    it('should create a new odl request and change the request state from "da accettare" to "Accettata" (which will be viewed as "Aperto") and then from "Aperto" to "Concluso" ', () => {
        dashboardPage.goToOdlPage()
        let newDesc = 'Ispezionare il ponte ' + makeRndmString(Math.floor(Math.random()*40) + 1)
        let priorita = 'Media'
        let cateogira = 'Ispezione'
        let responsabile = 'JET67BCHRT19SQ9L'
        let asset = 'AA-000001'
        odlPage.insertNewOdl(newDesc, priorita,cateogira, responsabile,asset)
        odlPage.changeOdlRequestStateWithCheck(' Da accettare ','Accettata', {descrizione:newDesc})
        cy.reload() //this reload is necessary bc the page respond too slowly 
        odlPage.changeOdlRequestStateWithCheck(' Aperto ','Concluso', {descrizione:newDesc})
    })

    it('should check that the right Responsabile of a "da accettare" ODL is displayed', () => {
        dashboardPage.goToOdlPage()
        let descrizione: string = 'UsarePerTestGestione'
        odlPage.checkResponsabileOfOdl('Giacomo',{descrizione: descrizione})
    }); // it works

    it('should check that the right Responsabile of a "open" ODL is displayed', () => {
        dashboardPage.goToOdlPage()
        let descrizione: string = 'Usare per test gestione ODL'
        odlPage.checkResponsabileOfOdl('Giacomo', {descrizione: descrizione})
    });

    it('should check that the right Personale of a "open" ODL is displayed', () => {
        dashboardPage.goToOdlPage()
        let descrizione: string = 'Usare per test gestione ODL'
        odlPage.checkPersonaleOfOdl('lkj2gyu34j4', {descrizione: descrizione})
    }); // it works

    it('should create a new manutentore and add it as new personale to an open odl and check if it is in the manutentori list associated to that odl', () => {
        dashboardPage.goToUsersPage()
        let email: string = makeRndmString(getRandInt(15)) + '@mail.cmms'
        let name: string = makeRndmString(getRandInt(15))
        let surname: string = 'cogId' + makeRndmString(getRandInt(10)) + makeRndmString(getRandInt(5))
        let residenza: string = makeRndmString(getRandInt(15))
        let codiceFiscale: string = makeRndmString(getRandInt(15))
        // let email: string = makeRndmString(getRandInt(15))
        let telefono: string = makeRndmString(getRandInt(15))
        let gerarchia: string = 'Tecnico'
        let ruolo: string = 'Meccanico'
        addNewUserMaintainer(gerarchia, name, surname, residenza, codiceFiscale, email, telefono, ruolo)
        goToOdlPage()
        odlPage.addPersonaleToOdl(surname, {descrizione: 'Usare per test gestione ODL'})  
        cy.reload()
        odlPage.checkManutentoreInResponsabili(codiceFiscale, {descrizione: 'Usare per test gestione ODL'})
    }); //it works !!

    it('should create a new odl with time Infos associated', () => {
        dashboardPage.goToOdlPage()
        let descrizProva = 'to do : ' + makeRndmString(Math.floor(Math.random()*25) + 1)
        odlPage.insertNewOdl(descrizProva, 'Alta', 'Correttiva','JET67BCHRT19SQ9L', 'AA-000001', 'note random : ' + makeRndmString(Math.floor(Math.random()*10) + 1) 
            , {startDate: '03/02/2024', conclusionDate: '03/05/2024', prevDuration: '1:45', startHour: '9:30'})
        odlPage.searchOdlAndExpectedTotal(1,{descrizione: descrizProva, priorita: 'Alta', tipologia: 'Correttiva'})
    });
})

//PLANNED MAINTENANCE Page      => MISSING 'Search' feature
describe('should test planned maintenance page to work as intended', () => {
    it('should create a new planned maintenance starting 24/04/2024, finishin 28/08/2025 with 2 week frequency, with a duration of 1h 45min and starting at 09:30 am (with floating option)', () => {
        dashboardPage.goToPlannedMaintenancePage()
        plannedMaintenancePage.createNewPlannedMaintenance('Operazione cy ' + makeRndmString(Math.floor(Math.random()*10) + 1), 1,
         'ST-472929', 'K28DGTY9SKW2YH7O' , 'settimanale', '2', '24/04/2024', '28/08/2025', '1:45', '9:30', true)
    }); 

    it('should create a planne dmaintenance with only necessary datas (monthly frequency starting the 4th of the month)', () => {
        dashboardPage.goToPlannedMaintenancePage()
        plannedMaintenancePage.createNewPlannedMaintenance('Operazione cy ' + makeRndmString(Math.floor(Math.random()*10) + 1), 1,'ST-472929', 'K28DGTY9SKW2YH7O', 'mensile', '1', '4/10/2024')
    })
})

//ASSET Page
describe('should test asset page to work as intended', () => {
    it('should create a new asset', () => {
        dashboardPage.goToAssetPage()
        assetPage.addNewAsset(makeRndmString(2) + '-' + getRandInt(100000).toString(), 'Descrizione : ' + makeRndmString(Math.random()*10 + 1), 'Elettrica', 'Attivo', 'Officina Xcorp', 'Piano Terra', 'officina vera e propria')
    }) //it works

    it('should search perform some researches through assets, and expect a total from a certain research', () => {
        dashboardPage.goToAssetPage()
        assetPage.searchAsset({toSearch: 'AA-000001', tutti: true, categoria: 'Elettrica', strutture: 'Seconda sede dell\'azienda XCorp'})     //es: (' ALT-92746FGU39E2 ', false, 'pippo', 'inserimento')
        assetPage.searchAsset({toSearch: 'AA-000001'})
        assetPage.searchAsset({categoria: 'Elettrica'})
        assetPage.searchAssetAndExpectedTotal(15, {categoria: 'Elettrica'})
    })  //test works (obv params have to make sense!!)

    it('should substitute an existing asset', () => {
        dashboardPage.goToAssetPage()
        let newNumSeriale = 'AA-' + makeRndmString(5)
        let sameDescr = 'descrstatic'   
        let randMotivo = makeRndmString(15)
        assetPage.sostituisciAsset(newNumSeriale, sameDescr, 'Elettrica', 'Sostituito', randMotivo, {toSearch: sameDescr})
    });
})

//CLIENTI Page
describe('should test Customer page to work as intended', () => {
    it('should create a new customer', () => {
        dashboardPage.goToCustomerPage()
        customerPage.addCustomer(makeRndmString(Math.random()*10 + 1) + ' Srl', 'cymail@' + makeRndmString(Math.random()*10 + 1) + '.cmms', '1234567', makeRndmString(Math.random()*10 + 1), makeRndmString(Math.random()*10 + 1), makeRndmString(Math.random()*10 + 1), 'cydesc ' + makeRndmString(Math.random()*10 + 1))
    });

    it('should search for a certain customer, in different ways, but with the same result', () => {
        dashboardPage.goToCustomerPage()
        customerPage.searchCustomerAndExpectedTotal(1,{struttura: 'Seconda sede dell\'azienda XCorp'})
        cy.reload()
        customerPage.searchCustomerAndExpectedTotal(1,{generalString: 'xcorp@auto.iis'})
        cy.reload()
        customerPage.searchCustomerAndExpectedTotal(1,{cliente: 'descrizione officina xcorp'})
    })

    it('should create a new customer and research it', () => {
        let name = makeRndmString(Math.random()*10 + 1) + ' Srl'
        let mail = 'cymail@' + makeRndmString(Math.random()*10 + 1) + '.cmms'
        let phoneN = '22345623'
        let codFisc = makeRndmString(Math.random()*10 + 1) 
        let pIva = makeRndmString(Math.random()*10 + 1)
        let fattIndirizzo = makeRndmString(Math.random()*10 + 1)
        let description = 'cy desc' + makeRndmString(Math.random()*10 + 1)
        dashboardPage.goToCustomerPage()
        customerPage.addCustomer(name, mail, phoneN, codFisc, pIva, fattIndirizzo, description)
        customerPage.searchCustomerAndExpectedTotal(1,{generalString: name})
    });

    it('should look for a customer (identified by the email "xcorp@auto.iis") and add a new randomized structure to it', () => {
        dashboardPage.goToCustomerPage()
        customerPage.addStructureToCustomer('nazione ' + makeRndmString(Math.random()*10 + 1), 'city ' + makeRndmString(Math.random()*10 + 1), 'indirizzo ' + makeRndmString(Math.random()*10 + 1), '16000', true, 'descrizione ' + makeRndmString(Math.random()*10 + 1), {generalString: 'xcorp@auto.iis'})
    });

    it('should check that a certain structure is owned/associated to a certain customer, exactly if Officina Xcorp is owned by Officina Automobilistica XCorp', () => {
        dashboardPage.goToCustomerPage()
        customerPage.checkIfStructureIsOwnedByCertainCustomer('Officina Xcorp', {generalString: 'Officina Automobilistica XCorp'})
    })
})

// STRUCTURE Page       => MISSING 'Associa piano' feature and what ne consegue *************
describe('should test structure page to work as intended', () => {
    
    it('should search for a certain structure and then perform the same research, expecting exactly 1 result ', () => {
        dashboardPage.goToStructurePage()
        structurePage.searchStructureAndExpectedTotal(1, {perStruttura: 'Seconda sede dell\'azienda XCorp'})
    });

})

describe('should test fornitori page to wark as intended', () => {
    it('should add a new fornitore', () => {
        dashboardPage.goToSupplierPage()
        fornitoriPage.addNewFornitore('cytest' + makeRndmString(Math.random()*10 + 1), makeRndmString(Math.random()*10 + 1), 'codfisc ' + makeRndmString(Math.random()*10 + 1), makeRndmString(Math.random()*10 + 1), 'indirizz' + makeRndmString(Math.random()*10 + 1),'desCy' + makeRndmString(Math.random()*10 + 1), makeRndmString(Math.random()*10 + 1) + '@mailtest.cy')
    }); // it works

    it('should research for a specific fornitore', () => {
        dashboardPage.goToSupplierPage()
        fornitoriPage.searchFornitoreWithExpectedNum(1,{fornitore: 'fornitest'})
    });
})

// MAINTAINERS PAGE
describe('should test manutentori page to work as intended', () => {
    it('should perform a research and expect 1 manutentore with those given infos', () => {
        dashboardPage.goToManutentoriPage()
        manutentoriPage.searchManutentoreAndExpectTotal(1,{generalSearch: 'Orlando'})
    })

    it('should change infos of a maintainer', () => {
        dashboardPage.goToManutentoriPage()
        manutentoriPage.modifyMantainer({nome:'Orlando', cognome:'Marchesi', residenza:'Genova', codFiscale: makeRndmString(getRandInt(5)), email:'orlandoMarchesi@dominio.it', telefono: '3729583461'}, {generalSearch:'Orlando'})
        cy.reload()
        manutentoriPage.searchManutentoreAndExpectTotal(1,{generalSearch: 'Orlando'})
    });

    it('should check if a role and a skill are associated to a certain maintainer', () => {
        dashboardPage.goToManutentoriPage() //codFiscale = lkj2gyu34j4, ruolo = Meccanico, skill = Inglese C2 
        manutentoriPage.checkIfRoleAssociatedToMaintainer(['Meccanico'], {generalSearch: 'lkj2gyu34j4'})
        cy.reload()
        manutentoriPage.checkIfSkillAssociatedToMaintainer(['Inglese C2'], {generalSearch: 'lkj2gyu34j4'})
    }); // it works

})

describe('should test roles page to work as intended', () => {
  it('should add a new role "Idraulico" (not associated to any manutentore), search for it and then delete it', () => {
      let newRole: string = 'Idraulico' + makeRndmString(7)
      let roleSector: string = 'Settore idraulico' + makeRndmString(7)
      dashboardPage.goToRolesPage()
      rolePage.addRuolo(newRole, roleSector/*, ['Mario', 'Luisa']*/)
      rolePage.searchRuoloWithExpectedNumber(1,{generalSearch: roleSector})
      cy.reload()
      rolePage.deactivateRuolo({generalSearch: roleSector})
  });  

  it('should add a new role and modify its infos', () => {
      dashboardPage.goToRolesPage()
      let tipo: string = 'ruoloNew' + makeRndmString(7)
      let desc: string = 'descrizNew' + makeRndmString(7)
      rolePage.addRuolo(tipo, desc)
      let tipoChanged: string = tipo + '++'
      let descChanged: string = desc + '++'
      rolePage.modifyRuolo(tipoChanged, descChanged, {generalSearch: tipo})
  });   
  
})

describe.only('should test abilita page to work as intended', () => {
    it('should test search feature', () => {
        dashboardPage.goToSkillsPage()
        skillPage.searchSkillWithExpectedNum(3,{manutentore:'Rossi'}) 
    });
    //to make the following test 'independent', we should create a new manutentore and use him as associato
    it('should add a new skill, modify and then deactivate it', () => {
        dashboardPage.goToSkillsPage()
        let tipo = makeRndmString(getRandInt(5))
        let descrizione = makeRndmString(getRandInt(10))
        skillPage.addSkill(tipo, descrizione)
        cy.reload()
        skillPage.modifySkill(tipo+'new', descrizione+'new', {generalSearch: descrizione})
        cy.reload()
        skillPage.deactivateSkill({generalSearch: descrizione+'new'})
        // => this can't be done bc we can't search with generalSearch and so we need to associate a manutentore, but we can't delete a skill associated to any manutentore
    });

    it('should add a new skill associated to a maintainer and then modify it', () => {
        dashboardPage.goToSkillsPage()
        let tipo = makeRndmString(getRandInt(5))
        let descrizione = makeRndmString(getRandInt(10))
        let associati = ['Lorena']
        skillPage.addSkill(tipo, descrizione, associati)
        cy.reload()
        skillPage.modifySkill(tipo+'new', descrizione+'new', {generalSearch: descrizione})
    });
})
