import * as dashboardPage from "../../pageObjects/dashboardPage"
import * as usersPage from "../../pageObjects/usersPage"
import * as odlPage from "../../pageObjects/odlPage"
import * as plannedMaintenancePage from "../../pageObjects/plannedMaintenancePage"
import * as assetPage from "../../pageObjects/assetPage"
import * as customerPage from "../../pageObjects/clientiPage"
import * as structurePage from "../../pageObjects/structurePage"
import * as fornitoriPage from "../../pageObjects/fornitoriPage"
import * as manutentoriPage from "../../pageObjects/maintainersPage"
import * as rolePage from "../../pageObjects/ruoliPage"
import * as skillPage from "../../pageObjects/abilitaPage"

import { adminEmail, adminPassword, adminName, adminSurname, adminRole } from "../../support/utils"
import { makeRndmString, getRandInt, goToAssetHistoryPage, goToAssetPage, goToCustomerPage, 
    goToMPHistoryPage, goToManutentoriPage, goToODLHistoryPage, goToOdlPage, goToPlannedMaintenancePage, goToRolesPage, openOrCloseLateralMenu,
    goToSkillsPage, goToStructurePage, goToSupplierPage, goToUsersPage } from "../../pageObjects/utils"

const dashboardPageUrl: string = 'http://localhost:8091/dashboard'

const addNewUserMaintainer = (gerarchia: string, name: string, surname: string, residenza: string, codiceFiscale: string, email: string, telefono: string, ruolo?: string, abilita?: string) => {
    //manutentore = name + '-' + surname
    let manutentore: string = name + '-' + surname
    cy.reload()
    goToManutentoriPage()
    manutentoriPage.addManutentore(name, surname, codiceFiscale, email, {residenza: residenza, telefono: telefono, ruolo: ruolo, abilita: abilita})
    cy.reload()
    goToUsersPage()
    usersPage.addUserManutentore(gerarchia, manutentore)
    usersPage.searchUserWithExpectedNum(1, email, true)
}

describe('Test for user changes', () => {
    it.skip('should check that mantainers are wiithout an user associated ', () => {
        dashboardPage.goToUsersPage()
        usersPage.checkNotAssociatedMaintainerAreActuallyNot()
    })

    it('should check that when creating an user with a maintainer associated, email fields correspond', () => {
        dashboardPage.goToManutentoriPage()
        let allUsersObject: any
        let name = makeRndmString(15)
        let surname = makeRndmString(15)
        let codF = makeRndmString(10)
        let mail = makeRndmString(15) + '@mail.cmms'
        manutentoriPage.addManutentore(name, surname, codF, mail)
        let manutentore = name + '-' + surname
        cy.reload()
        goToUsersPage()
        cy.intercept('GET', 'http://localhost:8091/api/user/getAllUsers').as('getAllUsers')
        usersPage.addUserManutentore('Tecnico', manutentore)
        cy.reload()
        cy.wait('@getAllUsers').then((intercepted) => {
            if(intercepted.response) {
                allUsersObject = intercepted.response.body
                console.log(typeof allUsersObject, 'All Users :', allUsersObject)
            }
            let flagIsThere: boolean = false
            allUsersObject!.forEach((user: { maintainer: { email: any }; username: string }) => {
                console.log('user.maintainer.email nel foreach errorista', user?.maintainer?.email)
                if(user?.maintainer?.email == mail && user?.username == mail)
                    flagIsThere = true
                })

            if(!flagIsThere)
                throw new Error('Mail don\'t correspond')
        })
    });

    it('should check that modifying mail of a maintainer, the user associated will be deactived', () => {
        let name = makeRndmString(15)
        let surname = makeRndmString(15)
        let codF = 'cf' + makeRndmString(8)
        let email = makeRndmString(15) + '@mail.cmms'
        addNewUserMaintainer('Tecnico', name, surname, 'ResidencE', codF, email, '2345611')
        /* check that user and maintainer are updated  */        
        cy.reload()
        goToUsersPage()
        cy.reload()
        usersPage.searchUserWithExpectedNum(1, email, true)
        cy.reload()
        goToManutentoriPage()
        manutentoriPage.searchManutentoreAndExpectTotal(1, {generalSearch: email})
        cy.reload()
        goToManutentoriPage()
        cy.reload()
        manutentoriPage.modifyMantainer({nome: name, cognome: surname, codFiscale: codF, email: 'cambio@gmail.com'}, {generalSearch: name})
        cy.reload()
        goToUsersPage()
        cy.reload()
        usersPage.searchUserWithExpectedNum(0, email, true)
        cy.reload()
        usersPage.searchUserWithExpectedNum(1, email, false)
    });

    it.only('should check that if a maintainer is deactivated, its associated user is deactivated too', () => {
        let name = makeRndmString(15)
        let surname = makeRndmString(15)
        let codF = 'cf' + makeRndmString(8)
        let email = makeRndmString(15) + '@mail.cmms'
        addNewUserMaintainer('Tecnico', name, surname, 'ResidencE', codF, email, '2345611')
        /* check that user and maintainer are updated  */        
        cy.reload()
        goToUsersPage()
        cy.reload()
        usersPage.searchUserWithExpectedNum(1, email, true)
        cy.reload()
        goToManutentoriPage()
        cy.reload()
        manutentoriPage.searchManutentoreAndExpectTotal(1, {generalSearch: email})

        cy.reload()
        manutentoriPage.deactivateManutentore(email)
        cy.reload()
        manutentoriPage.searchManutentoreAndExpectTotal(0, {generalSearch: email, attivi: true})
        cy.reload()
        manutentoriPage.searchManutentoreAndExpectTotal(1, {generalSearch: email, attivi: false})
    });
})