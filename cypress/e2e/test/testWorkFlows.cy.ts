import * as dashboardPage from "../../pageObjects/dashboardPage"
import * as usersPage from "../../pageObjects/usersPage"
import * as odlPage from "../../pageObjects/odlPage"
import * as plannedMaintenancePage from "../../pageObjects/plannedMaintenancePage"
import * as assetPage from "../../pageObjects/assetPage"
import * as customerPage from "../../pageObjects/clientiPage"
import * as structurePage from "../../pageObjects/structurePage"
import * as fornitoriPage from "../../pageObjects/fornitoriPage"
import * as manutentoriPage from "../../pageObjects/maintainersPage"
import * as rolePage from "../../pageObjects/ruoliPage"
import * as skillPage from "../../pageObjects/abilitaPage"

import { makeRndmString, getRandInt, goToAssetHistoryPage, goToAssetPage, goToCustomerPage, 
    goToMPHistoryPage, goToManutentoriPage, goToODLHistoryPage, goToOdlPage, goToPlannedMaintenancePage, goToRolesPage, openOrCloseLateralMenu,
    goToSkillsPage, goToStructurePage, goToSupplierPage, goToUsersPage } from "../../pageObjects/utils"
import { adminEmail, adminPassword, adminName, adminSurname, adminRole } from "../../support/utils"

enum priorities {'Bassa', 'Media', 'Alta'}

const addNewUserMaintainer = (gerarchia: string, name: string, surname: string, residenza: string, codiceFiscale: string, email: string, telefono: string, ruolo?: string, abilita?: string) => {
    //manutentore = name + '-' + surname
    let manutentore: string = name + '-' + surname
    cy.reload()
    goToManutentoriPage()
    manutentoriPage.addManutentore(name, surname, codiceFiscale, email, {residenza: residenza, telefono: telefono, ruolo: ruolo, abilita: abilita})
    cy.reload()
    goToUsersPage()
    usersPage.addUserManutentore(gerarchia, manutentore)
    usersPage.searchUserWithExpectedNum(1, email, true)
}


/* create a new user (maintainer) and a new asset and create a new ODL (Richiesta Intervento) assigned to those new asset and user, accept it and open it */
describe('**WorkFlow 1**', () => {
    it('should create a new user(maintainer) and a new asset then create a new odl assigned to the new asset and user, and then accept it ("Da accettare" -> "Accettata" -> "Aperto")', () => {
        let email: string = 'mail@' + makeRndmString(getRandInt(20)) + '.cmms' 
        let name: string = 'name'+ makeRndmString(getRandInt(20))   
        let surname: string = 'surname' + makeRndmString(getRandInt(20))
        let codFisc: string = 'CF' + makeRndmString(getRandInt(10))
        addNewUserMaintainer('Tecnico', name, surname, 'ResidenZa', codFisc, email, '2378998743', 'Meccanico', 'Patente C')
        cy.reload()
        goToAssetPage()
        let nomeAsset: string = 'asset' + makeRndmString(getRandInt(20))
        assetPage.addNewAsset(nomeAsset, 'descrizione', 'Elettrica', 'Attivo', 'Officina Xcorp', 'Piano Terra', 'officina vera e propria')
        cy.reload()
        assetPage.searchAssetAndExpectedTotal(1, {toSearch: nomeAsset})
        cy.reload()
        goToOdlPage()
        let descrizione = 'descrizione' + makeRndmString(getRandInt(20))
        odlPage.insertNewOdl(descrizione, 'Media', 'Correttiva', name, nomeAsset)
        cy.reload()
        odlPage.searchOdlAndExpectedTotal(1, {descrizione: descrizione})
        cy.reload()
        odlPage.changeOdlRequestStateWithCheck('Da accettare', 'Accettata', {descrizione: descrizione})
        cy.reload()
        odlPage.changeOdlRequestStateWithCheck('Aperto', 'Concluso', {descrizione: descrizione})
    })
})

/* create a new user (maintainer) and a new asset and create a new ODL (Richiesta Intervento) assigned to those new asset and user and refuse it */
describe.only('**WorkFlow 2**', () => { 
    it('should create a new user(maintainer) and a new asset then create a new odl assigned to the new asset and user, and then refuse it ("Da accettare" -> "Rifiutata") ', () => {
        dashboardPage.goToUsersPage()
        let email: string = 'mail@' + makeRndmString(getRandInt(10)) + '.cmms' 
        let name: string = 'name'+ makeRndmString(getRandInt(20))   
        let surname: string = 'surname' + makeRndmString(getRandInt(20))
        let codFisc: string = 'CF' + makeRndmString(getRandInt(10))
        addNewUserMaintainer('Tecnico', name, surname, 'ResidenZa', codFisc, email, '2378998743', 'Meccanico', 'Patente C')
        cy.reload()
        goToAssetPage()
        let nomeAsset: string = 'asset' + makeRndmString(getRandInt(20))
        assetPage.addNewAsset(nomeAsset, 'descrizione', 'Elettrica', 'Attivo', 'Officina Xcorp', 'Piano Terra', 'officina vera e propria')
        cy.reload()
        assetPage.searchAsset({toSearch: nomeAsset})
        cy.reload()
        goToOdlPage()
        let descrizione = 'descrizione' + makeRndmString(getRandInt(20))
        odlPage.insertNewOdl(descrizione, 'Media', 'Correttiva', name, nomeAsset)
        cy.reload()
        odlPage.searchOdlAndExpectedTotal(1, {descrizione: descrizione})
        cy.reload()
        odlPage.changeOdlRequestStateWithCheck('Da accettare', 'Rifiutata', {descrizione: descrizione})
    }) 
})

/* create a new user and a new asset and create a new Planned Maintenance  */
describe('**WorkFlow 3**', () => {
    it('should create a new user and a new asset and then create a new Planned Maintenance (with only conclusion date)', () => {
        dashboardPage.goToUsersPage()
        let email: string = 'mail@' + makeRndmString(getRandInt(20)) + '.cmms' 
        let name: string = 'name'+ makeRndmString(getRandInt(20))   
        let surname: string = 'surname' + makeRndmString(getRandInt(20))
        let codFisc: string = 'CF' + makeRndmString(getRandInt(10))
        addNewUserMaintainer('Tecnico', name, surname, 'ResidenZa', codFisc, email, '2378998743', 'Meccanico', 'Patente C')
        cy.reload()
        goToAssetPage()
        let nomeAsset: string = 'asset' + makeRndmString(getRandInt(20))
        assetPage.addNewAsset(nomeAsset, 'descrizione', 'Elettrica', 'Attivo', 'Officina Xcorp', 'Piano Terra', 'officina vera e propria')
        cy.reload()
        assetPage.searchAsset({toSearch: nomeAsset})
        cy.reload()
        goToPlannedMaintenancePage()
        let pmDescription: string = 'descrizione' + makeRndmString(getRandInt(20))
        let pmPriority: priorities = 1
        let startDay: string = '3'
        let conclusionDay: string = '28'
        let frequencyType: string = 'settimanale'
        plannedMaintenancePage.createNewPlannedMaintenance(pmDescription, pmPriority, nomeAsset, name, frequencyType, startDay, conclusionDay)
    });
})

/* create a new customer and associate to him a new structure */
describe('**WorkFlow 4**', () => {
    it('should create a new customer and associate a new structure to it', () => {
        dashboardPage.goToCustomerPage()
        let customerName: string = 'customer' + makeRndmString(getRandInt(20)) 
        let customerEmail: string = 'customerEmail' + makeRndmString(getRandInt(20)) + '@mail.cmms'
        let customerTelefono: string = makeRndmString(getRandInt(20))
        let customerCodFisc: string = 'CF' + makeRndmString(getRandInt(10))
        let customerPIva: string =  makeRndmString(getRandInt(14))
        let customerFattIndirizzo: string = 'Via' + makeRndmString(getRandInt(20))
        let customerDescription: string = 'descript-' + makeRndmString(getRandInt(20))
        customerPage.addCustomer(customerName, customerEmail, customerTelefono, customerCodFisc, customerPIva, customerFattIndirizzo, customerDescription)
        cy.reload()
        let nazione: string = 'Italia'
        let citta: string = 'citta' + makeRndmString(getRandInt(20))
        let indirizzo: string = 'indirizzo' + makeRndmString(getRandInt(20))
        let codicePostale: string = 'CP' + makeRndmString(getRandInt(20))
        let isCentral: boolean = true
        let descrizione: string = 'strutturaDescript' + makeRndmString(getRandInt(20))
        customerPage.addStructureToCustomer(nazione, citta, indirizzo, codicePostale, isCentral, descrizione, {generalString: customerName})
    });
})

/* create a new customer and associate to him a new structure (with different number of floors anda areas associated to that structure) */
describe('**WorkFlow 5**', () => {
    it('should create a new customer, and associate to it a new structure with a floor associated, which contains an area', () => {
        dashboardPage.goToCustomerPage()
        let customerName: string = 'customer' + makeRndmString(getRandInt(20)) 
        let customerEmail: string = 'customerEmail' + makeRndmString(getRandInt(20)) + '@mail.cmms'
        let customerTelefono: string = makeRndmString(getRandInt(20))
        let customerCodFisc: string = 'CF' + makeRndmString(getRandInt(10))
        let customerPIva: string =  makeRndmString(getRandInt(14))
        let customerFattIndirizzo: string = 'Via' + makeRndmString(getRandInt(20))
        let customerDescription: string = 'descript-' + makeRndmString(getRandInt(20))
        customerPage.addCustomer(customerName, customerEmail, customerTelefono, customerCodFisc, customerPIva, customerFattIndirizzo, customerDescription)
        cy.reload()
        let nazione: string = 'Italia'
        let citta: string = 'citta' + makeRndmString(getRandInt(20))
        let indirizzo: string = 'indirizzo' + makeRndmString(getRandInt(20))
        let codicePostale: string = makeRndmString(getRandInt(10))
        let isCentral: boolean = true
        let descrizione: string = 'strutturaDescript' + makeRndmString(getRandInt(20))
        customerPage.addStructureToCustomerWithFloor(nazione, citta, indirizzo, codicePostale, isCentral, descrizione, {generalString: customerName}, {numeroPiano: ['1'], descrizionePiano: ['primo piano']}, {tipologia:['A'], descrizioneArea: ['Area A'], pianoAssociato: ['1']} )
    });

    it('should create a new customer, and associate to it a new structure with a floor associated, which contains two areas', () => {
        dashboardPage.goToCustomerPage()
        let customerName: string = 'customer' + makeRndmString(getRandInt(20)) 
        let customerEmail: string = 'customerEmail' + makeRndmString(getRandInt(20)) + '@mail.cmms'
        let customerTelefono: string = makeRndmString(getRandInt(20))
        let customerCodFisc: string = 'CF' + makeRndmString(getRandInt(10))
        let customerPIva: string =  makeRndmString(getRandInt(14))
        let customerFattIndirizzo: string = 'Via' + makeRndmString(getRandInt(20))
        let customerDescription: string = 'descript-' + makeRndmString(getRandInt(20))
        customerPage.addCustomer(customerName, customerEmail, customerTelefono, customerCodFisc, customerPIva, customerFattIndirizzo, customerDescription)
        cy.reload()
        let nazione: string = 'Italia'
        let citta: string = 'citta' + makeRndmString(getRandInt(20))
        let indirizzo: string = 'indirizzo' + makeRndmString(getRandInt(20))
        let codicePostale: string = makeRndmString(getRandInt(9))
        let isCentral: boolean = true
        let descrizione: string = 'strutturaDescript' + makeRndmString(getRandInt(20))
        customerPage.addStructureToCustomerWithFloor(nazione, citta, indirizzo, codicePostale, isCentral, descrizione, {generalString: customerName}, {numeroPiano: ['1'], descrizionePiano: ['primo piano']}, {tipologia:['A','B'], descrizioneArea: ['Area A', 'Area B'], pianoAssociato: ['1','1']} )
    });

    it('should create a new customer, and associate to it a new structure with two floors associated, which contains two areas each ', () => {
        dashboardPage.goToCustomerPage()
        let customerName: string = 'customer' + makeRndmString(getRandInt(20)) 
        let customerEmail: string = 'customerEmail' + makeRndmString(getRandInt(20)) + '@mail.cmms'
        let customerTelefono: string = makeRndmString(getRandInt(20))
        let customerCodFisc: string = 'CF' + makeRndmString(getRandInt(10))
        let customerPIva: string =  makeRndmString(getRandInt(14))
        let customerFattIndirizzo: string = 'Via' + makeRndmString(getRandInt(20))
        let customerDescription: string = 'descript-' + makeRndmString(getRandInt(20))
        customerPage.addCustomer(customerName, customerEmail, customerTelefono, customerCodFisc, customerPIva, customerFattIndirizzo, customerDescription)
        cy.reload()
        let nazione: string = 'Italia'
        let citta: string = 'citta' + makeRndmString(getRandInt(20))
        let indirizzo: string = 'indirizzo' + makeRndmString(getRandInt(20))
        let codicePostale: string = makeRndmString(getRandInt(10))
        let isCentral: boolean = true
        let descrizione: string = 'strutturaDescript' + makeRndmString(getRandInt(20))
        customerPage.addStructureToCustomerWithFloor(nazione, citta, indirizzo, codicePostale, isCentral, descrizione, {generalString: customerName}, {numeroPiano: ['1','2'], descrizionePiano: ['primo piano', 'secondo piano']}, {tipologia:['A1', 'B1', 'A2', 'B2'], descrizioneArea: ['Area A1', 'Area B1', 'Area A2', 'Area B2'], pianoAssociato: ['1','1', '2', '2']} )
    });
})

/** create a new user, a new role and a new skill and associate them to the user during the user creation */
describe('**WorkFlow 6**', () => { 
    it('should create a new skill and a new role, and then create a new user with those associated, through users page (during creation)', () => {
        dashboardPage.goToSkillsPage()
        let nomeSkill = 'skill' + makeRndmString(getRandInt(20))
        let descrizioneSkill = 'descrizione' + makeRndmString(getRandInt(20))
        skillPage.addSkill( nomeSkill, descrizioneSkill)
        cy.reload()
        goToRolesPage()
        let nomeRuolo = 'ruolo' + makeRndmString(getRandInt(20))
        let descrizioneRuolo = 'descrizione' + makeRndmString(getRandInt(20))
        rolePage.addRuolo(nomeRuolo, descrizioneRuolo)
        cy.reload()
        goToUsersPage()
        let email: string = 'mail@' + makeRndmString(getRandInt(20)) + '.cmms' 
        let name: string = 'name'+ makeRndmString(getRandInt(20))   
        let surname: string = 'surname' + makeRndmString(getRandInt(20))
        let codFisc: string = 'CF' + makeRndmString(getRandInt(10))
        addNewUserMaintainer('Tecnico', name, surname, 'ResidenZa', codFisc, email, '2378998743', nomeRuolo, nomeSkill)
        usersPage.searchUserWithExpectedNum(1, surname, true)
    }); // it works
})

/** create a new user, a new role and a new skill and associate them to the maintainer through maintainers page */
describe('**WorkFlow 7**', () => {
    it('should create a new skill and a new role, and then create a new user with those associated, through maintainers page (modifying an exixsting maintainer)', () => {
        dashboardPage.goToSkillsPage()
        let nomeSkill = 'skill' + makeRndmString(getRandInt(20))
        let descrizioneSkill = 'descrizione' + makeRndmString(getRandInt(20))
        skillPage.addSkill( nomeSkill, descrizioneSkill)
        cy.reload()
        goToRolesPage()
        let nomeRuolo = 'ruolo' + makeRndmString(getRandInt(20))
        let descrizioneRuolo = 'descrizione' + makeRndmString(getRandInt(20))
        rolePage.addRuolo(nomeRuolo, descrizioneRuolo)
        cy.reload()
        goToUsersPage()
        let email: string = 'mail@' + makeRndmString(getRandInt(20)) + '.cmms' 
        let name: string = 'name'+ makeRndmString(getRandInt(20))   
        let surname: string = 'surname' + makeRndmString(getRandInt(20))
        let codFisc: string = 'CF' + makeRndmString(getRandInt(10))
        addNewUserMaintainer('Tecnico', name, surname, 'ResidenZa', codFisc, email, '2378998743')
        usersPage.searchUserWithExpectedNum(1, surname, true)
        cy.reload()
        goToManutentoriPage()
        manutentoriPage.addRoleToMaintainer(nomeRuolo,{generalSearch: codFisc})
        cy.reload()
        manutentoriPage.checkIfRoleAssociatedToMaintainer([nomeRuolo], {generalSearch: codFisc})
        cy.reload()
        manutentoriPage.addSkillToMaintainer(nomeSkill, {generalSearch: codFisc})
        cy.reload()
        manutentoriPage.checkIfSkillAssociatedToMaintainer([nomeSkill], {generalSearch: codFisc})
    }); // it works
})

/** create a new user, a new role and a new skill and associate them to the user through skills and roles page */
describe('**WorkFlow 8**', () => {
    it('should create a new skill and a new role, and then create a new user with those associated, through skills and roles page (adding the user as associato during the skill and role creation)', () => {
        let email: string = 'mail@' + makeRndmString(getRandInt(20)) + '.cmms' 
        let name: string = 'name'+ makeRndmString(getRandInt(20))   
        let surname: string = 'surname' + makeRndmString(getRandInt(20))
        let codFisc: string = 'CF' + makeRndmString(getRandInt(10))
        addNewUserMaintainer('Tecnico', name, surname, 'ResidenZa', codFisc, email, '2378998743')
        usersPage.searchUserWithExpectedNum(1, surname, true)
        cy.reload()
        goToSkillsPage()
        let nomeSkill = 'skill' + makeRndmString(getRandInt(20))
        let descrizioneSkill = 'descrizione' + makeRndmString(getRandInt(20))
        skillPage.addSkill( nomeSkill, descrizioneSkill, [name])
        cy.reload()
        goToRolesPage()
        let nomeRuolo = 'ruolo' + makeRndmString(getRandInt(20))
        let descrizioneRuolo = 'descrizione' + makeRndmString(getRandInt(20))
        rolePage.addRuolo(nomeRuolo, descrizioneRuolo, [name])
        cy.reload()
    }); // it works
})
