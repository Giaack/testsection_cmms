import { adminEmail, adminPassword, adminName, adminSurname, adminRole } from "../../support/utils"
import { makeRndmString, getRandInt, goToAssetHistoryPage, goToAssetPage, goToCustomerPage, 
    goToMPHistoryPage, goToManutentoriPage, goToODLHistoryPage, goToOdlPage, goToPlannedMaintenancePage, goToRolesPage, openOrCloseLateralMenu,
    goToSkillsPage, goToStructurePage, goToSupplierPage, goToUsersPage } from "../../pageObjects/utils"
import { loginPage } from "../../pageObjects/loginPageAsObject"

const dashboardPageUrl: string = 'http://localhost:8091/dashboard'

describe('Should test login to work as intended', () => {
    it('should check that login works as intended', () => {
        loginPage.visit()
        loginPage.performLogin(adminEmail, adminPassword)
        cy.url().should('equal', dashboardPageUrl)
    })
})