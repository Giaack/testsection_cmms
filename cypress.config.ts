import { defineConfig } from "cypress";

export default defineConfig({
  e2e: {
    baseUrl: 'http://localhost:8091',
    chromeWebSecurity: false
  },
  video: true
});
